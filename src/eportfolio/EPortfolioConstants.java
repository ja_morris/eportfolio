package eportfolio;

/**
 *
 * @author James
 */
public class EPortfolioConstants {

    //APP SPECIFIC INFORMATION
    public static String APP_TITLE = "ePortfolio Generator";
    public static String PATH_SITES = "./data/sites";
    public static String PATH_BUILDER_SITE = "./data/DefaultPresentation/";
    public static String PATH_IMAGES = "./images/";
    public static String PATH_ICONS = PATH_IMAGES + "icons/";
    public static String APPLICATION_ICON = "file:" + EPortfolioConstants.PATH_ICONS + "eportfolio.png";
    public static String PATH_MEDIA = PATH_IMAGES + "/media/";
    public static String PATH_SAVED = "./savedportfolio/";
    public static String DEFAULT_FOOTER = "this is the default footer.";

    //IMAGES
    public static String DEFAULT_IMAGE = "DefaultStartSlide.png";
    public static String DEFAULT_VIDEO = PATH_MEDIA + "DefaultVideo.mp4";

    //STYLESHEETS
    public static String PATH_CSS = "eportfolio/style/";
    public static String STYLE_SHEET_UI = PATH_CSS + "ePortfolioStyle.css";

    public static String[] FONT_TYPES = new String[]{"Font 1", "Font 2", "Font 3", "Font 4", "Font 5"};

    //FILE CONTROLLING TOOLBARD INFORMATION
    public static String BUTTON_SAVE_LOCATION = "Save.png";
    public static String BUTTON_LOAD_LOCATION = "Load.png";
    public static String BUTTON_EXPORT_LOCATION = "Export.png";
    public static String BUTTON_NEW_LOCATION = "New.png";
    public static String BUTTON_EXIT_LOCATION = "Exit.png";
    public static String CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON = "horizontal_toolbar_button";

    public static String SAVE_BUTTON_TEXT = "This button will save progress.";
    public static String LOAD_BUTTON_TEXT = "This button will laod previous progress.";
    public static String NEW_BUTTON_TEXT = "This button will create a new project.";
    public static String EXPORT_BUTTON_TEXT = "This button will export progress.";
    public static String EXIT_BUTTON_TEXT = "This button will exit the program.";

    //WORKSPACE CONSTANTS
    public static int MIN_BUTTON_SIZE = 64;
    public static String BUTTON_EDIT_ELEMENT_LOCATION = "Edit.png";
    public static String BUTTON_ADD_ELEMENT_LOCATION = "Add.png";
    public static String BUTTON_REMOVE_ELEMENT_LOCATION = "Remove.png";
    public static String BUTTON_MOVEUP_ELEMENT_LOCATION = "MoveUp.png";
    public static String BUTTON_MOVEDOWN_ELEMENT_LOCATION = "MoveDown.png";
    public static String BUTTON_TEXT_ELEMENT_LOCATION = "Text.png";
    public static String BUTTON_IMAGE_ELEMENT_LOCATION = "Picture.png";
    public static String BUTTON_VIDEO_ELEMENT_LOCATION = "Video.png";
    public static String BUTTON_LIST_ELEMENT_LOCATION = "List.png";
    public static String BUTTON_SLIDESHOW_ELEMENT_LOCATION = "SlideShow.png";
    public static String BUTTON_HEADER_ELEMENT_LOCATION = "Header.png";
    public static String BUTTON_MOVERIGHT_ELEMENT_LOCATION = "Right.png";
    public static String BUTTON_MOVELEFT_ELEMENT_LOCATION = "Left.png";

    public static String PAGE_TITLE_DEFAULT = "Untitled";

    public static String TEXT_ELEMENT_TITLE = "Text Editor";
    public static String TEXT_ELEMENT_DEFAULT = "This is the default text. Please edit to change this message.";
    public static String HEADER_ELEMENT_DEFAULT = "This is the default header message. Please edit to change this message.";
    public static String IMAGE_ELEMENT_TITLE = "Image Editor";
    public static String MEDIA_ELEMENT_DEFAULT = "This is a default caption";
    public static String VIDEO_ELEMENT_TITLE = "Video Editor";
    public static String SLIDESHOW_ELEMENT_TITLE = "SlideShow Editor";

    public static String ADD_PAGE_TOOLTIP = "This will add another page";
    public static String REMOVE_PAGE_TOOLTIP = "This will remove the current page.";
    public static String MOVELEFT_PAGE_TOOLTIP = "This will move the page left.";
    public static String MOVERIGHT_PAGE_TOOLTIP = "This will move the page right.";

    public static String EDIT_ELEMENT_TOOLTIP = "Launch a menu to edit your selected element.";
    public static String REMOVE_ELEMENT_TOOLTIP = "Will remove the current selected element.";
    public static String MOVEUP_ELEMENT_TOOLTIP = "Will move the current selected element up in the list.";
    public static String MOVEDOWN_ELEMENT_TOOLTIP = "Will move the current selected element down in the list.";

    public static String TEXT_ELEMENT_TOOLTIP = "Adds a new text element.";
    public static String VIDEO_ELEMENT_TOOLTIP = "Adds a new video element.";
    public static String IMAGE_ELEMENT_TOOLTIP = "Adds a new picture element.";
    public static String SLIDESHOW_ELEMENT_TOOLTIP = "Adds a new SlideShow element.";
    public static String LIST_ELEMENT_TOOLTIP = "Adds a new list element.";
    public static String HEADER_ELEMENT_TOOLTIP = "Adds a new heading element.";

    public static String TEXT_HYPERLINK_HELP = "Please type <hyperlinkOpen>text: this is a test link: this is the link. <hperlinkClose> to add a hyperlink";

    public static String DEFAULT_LIST_ELEMENT_COMPONENT = "This is a list component.";
    public static String DEFAULT_LIST_ELEMENT_TITLE = "Untitled List";
    public static String EMPTY_LIST_COMPONENT = "This list is currently empty";

    public static String SITEVIEW_ICON_LOCATION = "SiteView.png";
    public static String WEBVIEW_ICON_LOCATION = "WebView.png";

    public static String SITEVIEW_ELEMENT_TOOLTIP = "Edit the portfolio";
    public static String WEBVIEW_ELEMENT_TOOLTIP = "View the portfolio here.";

    public static String SAVE_AS_LOCATION = "SaveAs.png";
    public static String SAVE_AS_TOOLTIP = "This will save the file in a specific location";
    public static String DEFAULT_TITLE = "DEFAULT";

}
