package eportfolio.View;

import eportfolio.Controller.EPortfolioFileManager;
import eportfolio.EPortfolio;
import eportfolio.EPortfolioConstants;
import eportfolio.Model.EPortfolioModel;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebView;
import javafx.stage.Screen;
import javafx.stage.Stage;

/**
 *
 * @author James
 */
public class WebSpace extends VBox {

    private EPortfolioModel ePortfolio;
    private WebView browser;
    private VBox housing;

    public WebSpace(EPortfolioModel current) {
        this.ePortfolio = current;
        browser = new WebView();
        housing = new VBox();
        this.getChildren().add(housing);
        repaint();
    }

    public void repaint() {
        housing.getChildren().clear();
        generateJavaScript();
    }

    private void generateJavaScript() {
        if (ePortfolio.getPages().isEmpty()) {
            return;
        }
        File file = new File(EPortfolioConstants.PATH_BUILDER_SITE + "/DefaultPresentation/index.html");
        Rectangle2D primaryScreenBounds = Screen.getPrimary().getVisualBounds();
        browser.setMinWidth(primaryScreenBounds.getWidth() / 2);
        browser.setMinHeight(primaryScreenBounds.getHeight() * .9);
        StackPane container = new StackPane();
        System.out.print(file.exists());
        try {
            browser.getEngine().load(EPortfolioFileManager.updateWebPortfolio(EPortfolioConstants.PATH_SITES, ePortfolio).toString());
        } catch (MalformedURLException ex) {
            Logger.getLogger(WebSpace.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(WebSpace.class.getName()).log(Level.SEVERE, null, ex);
        }

        container.getChildren().add(browser);
        housing.getChildren().add(container);
    }

}
