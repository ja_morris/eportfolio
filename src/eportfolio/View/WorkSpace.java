package eportfolio.View;

import FXMLControllers.PageInformationController;
import FXMLControllers.SlideController;
import eportfolio.Controller.GeneralSelectionController;
import eportfolio.Controller.ImageSelectionController;
import eportfolio.EPortfolioConstants;
import eportfolio.Elements.HeaderElement;
import eportfolio.Elements.ImageElement;
import eportfolio.Elements.ListElement;
import eportfolio.Elements.PageElement;
import eportfolio.Elements.SlideShowElement;
import eportfolio.Elements.TextElement;
import eportfolio.Elements.VideoElement;
import eportfolio.Model.EPortfolioModel;
import eportfolio.Model.Page;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Screen;

/**
 *
 * @author James
 */
public class WorkSpace extends VBox {

    private HBox pages, pageTools, workArea, pageHousing;
    private VBox elements, moveTools, menuTools, editor, pageInformation;
    private ScrollPane pageArea;
    private EPortfolioModel ePortfolio;
    private Button editElement, removeElement, moveElementUp, moveElementDown;
    private Button textElement, imageElement, videoElement, slideShowElement, listElement, headerElement;
    private Button addPage, removePage, movePageLeft, movePageRight;

    public WorkSpace(EPortfolioModel ePortfolio) {
        this.ePortfolio = ePortfolio;
        initializeElements();
    }

    public EPortfolioModel getEPortfolio() {
        return ePortfolio;
    }

    private void initializeElements() {
        pageHousing = new HBox();
        pages = new HBox();
        pages.setStyle("-fx-background-color: #00A638");
        pageTools = new HBox();
        elements = new VBox();
        elements.setStyle("-fx-background-color: #FFD146");
        moveTools = new VBox();
        menuTools = new VBox();
        editor = new VBox();
        pageInformation = new VBox();

        Screen screen = Screen.getPrimary();
        Rectangle2D bounds = screen.getVisualBounds();
        ScrollPane panel = new ScrollPane(elements);
        panel.setStyle("-fx-background-color: #FFD146");

        panel.setMinWidth(bounds.getWidth() / 2);
        panel.setMinHeight(bounds.getHeight() / 8);

        pageArea = new ScrollPane(pages);
        pageArea.setMinWidth(bounds.getWidth() / 2.2);
        pageArea.setMaxWidth(bounds.getWidth() / 2.2);
        pageArea.setMinHeight(EPortfolioConstants.MIN_BUTTON_SIZE);

        pageHousing.getChildren().addAll(pageArea, pageTools);

        workArea = new HBox();
        workArea.getChildren().addAll(moveTools, panel, menuTools);
        this.getChildren().addAll(pageHousing, workArea, editor, pageInformation);
        paint();
    }

    public void paint() {
        clearElements();
        paintPages();
        paintToolbars();
        paintElements();
        paintPageInformation();
        activateToolbars();
    }

    private void paintPages() {
        Button pageBtn;
        for (Page temp : ePortfolio.getPages()) {
            pageBtn = new Button(temp.getTitle());
            pageBtn.setMinHeight(EPortfolioConstants.MIN_BUTTON_SIZE);
            pageBtn.setOnAction(e -> {
                ePortfolio.selectElement(temp);
                paint();
            });
            pages.getChildren().add(pageBtn);
        }
    }

    private void paintPageInformation() {
        if (ePortfolio.getCurrentPageSelected() == null) {
            return;
        }

        AnchorPane pane;
        FXMLLoader loader;
        PageInformationController headController;
        try {
            pane = (loader = new FXMLLoader(getClass().getResource("/FXMLControllers/PageInformation.fxml"))).load();
            headController = loader.getController();
        } catch (IOException ex) {
            Logger.getLogger(HeaderElement.class.getName()).log(Level.SEVERE, null, ex);
            return;
        }
        headController.initializeVariables(ePortfolio);
        pageInformation.getChildren().add(pane);
        Screen screen = Screen.getPrimary();
        Rectangle2D bounds = screen.getVisualBounds();
        pageInformation.setMinWidth(bounds.getWidth() / 2.2);
        pageInformation.setMaxWidth(bounds.getWidth() / 2.2);
        pageInformation.getStyleClass().add("settings");
    }

    private void activateToolbars() {
        removeElement.setOnAction(e -> {
            if (ePortfolio.getCurrentPageSelected() == null) {
                return;
            }
            ePortfolio.getCurrentPageSelected().removeElement();
            paint();
        });
        moveElementDown.setOnAction(e -> {
            if (ePortfolio.getCurrentPageSelected() == null) {
                return;
            }
            ePortfolio.getCurrentPageSelected().moveElementDown();
            paint();
        });
        moveElementUp.setOnAction(e -> {
            if (ePortfolio.getCurrentPageSelected() == null) {
                return;
            }
            ePortfolio.getCurrentPageSelected().moveElementUp();
            paint();
        });
        editElement.setOnAction(e -> {
            if (ePortfolio.getCurrentPageSelected() == null) {
                return;
            }
            ePortfolio.getCurrentPageSelected().
                    getCurrentElementSelected().generateWorkHousing();
        });

        textElement.setOnAction(e -> {
            TextElement temp = new TextElement();
            ePortfolio.getCurrentPageSelected().addElement(temp);
            ePortfolio.getControl().markFileAsNotSaved();
            paint();
        });
        imageElement.setOnAction(e -> {
            ImageElement temp = new ImageElement();
            ePortfolio.getCurrentPageSelected().addElement(temp);
            ePortfolio.getControl().markFileAsNotSaved();
            paint();
        });
        videoElement.setOnAction(e -> {
            VideoElement temp = new VideoElement();
            ePortfolio.getCurrentPageSelected().addElement(temp);
            ePortfolio.getControl().markFileAsNotSaved();
            paint();
        });
        headerElement.setOnAction(e -> {
            HeaderElement temp = new HeaderElement();
            ePortfolio.getCurrentPageSelected().addElement(temp);
            ePortfolio.getControl().markFileAsNotSaved();
            paint();
        });
        listElement.setOnAction(e -> {
            ListElement temp = new ListElement();
            ePortfolio.getCurrentPageSelected().addElement(temp);
            ePortfolio.getControl().markFileAsNotSaved();
            paint();
        });
        slideShowElement.setOnAction(e -> {
            SlideShowElement temp = new SlideShowElement();
            ePortfolio.getCurrentPageSelected().addElement(temp);
            ePortfolio.getControl().markFileAsNotSaved();
            paint();
        });

        addPage.setOnAction(e -> {
            ePortfolio.addNewPage();
            ePortfolio.getControl().markFileAsNotSaved();
            this.paint();
        });
        removePage.setOnAction(e -> {
            ePortfolio.removeElement();
            ePortfolio.getControl().markFileAsNotSaved();
            this.paint();
        });
        movePageLeft.setOnAction(e -> {
            ePortfolio.movePageUp();
            this.paint();
        });
        movePageRight.setOnAction(e -> {
            ePortfolio.movePageDown();
            this.paint();
        });
    }

    private void paintToolbars() {
        editElement = initChildButton(moveTools, EPortfolioConstants.BUTTON_EDIT_ELEMENT_LOCATION,
                EPortfolioConstants.EDIT_ELEMENT_TOOLTIP, EPortfolioConstants.CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON,
                false);
        removeElement = initChildButton(moveTools, EPortfolioConstants.BUTTON_REMOVE_ELEMENT_LOCATION,
                EPortfolioConstants.REMOVE_ELEMENT_TOOLTIP, EPortfolioConstants.CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON,
                false);
        moveElementUp = initChildButton(moveTools, EPortfolioConstants.BUTTON_MOVEUP_ELEMENT_LOCATION,
                EPortfolioConstants.MOVEUP_ELEMENT_TOOLTIP, EPortfolioConstants.CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON,
                false);
        moveElementDown = initChildButton(moveTools, EPortfolioConstants.BUTTON_MOVEDOWN_ELEMENT_LOCATION,
                EPortfolioConstants.MOVEDOWN_ELEMENT_TOOLTIP, EPortfolioConstants.CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON,
                false);

        headerElement = initChildButton(menuTools, EPortfolioConstants.BUTTON_HEADER_ELEMENT_LOCATION,
                EPortfolioConstants.HEADER_ELEMENT_TOOLTIP, EPortfolioConstants.CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON,
                false);
        textElement = initChildButton(menuTools, EPortfolioConstants.BUTTON_TEXT_ELEMENT_LOCATION,
                EPortfolioConstants.MOVEDOWN_ELEMENT_TOOLTIP, EPortfolioConstants.CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON,
                false);
        imageElement = initChildButton(menuTools, EPortfolioConstants.BUTTON_IMAGE_ELEMENT_LOCATION,
                EPortfolioConstants.IMAGE_ELEMENT_TOOLTIP, EPortfolioConstants.CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON,
                false);
        videoElement = initChildButton(menuTools, EPortfolioConstants.BUTTON_VIDEO_ELEMENT_LOCATION,
                EPortfolioConstants.VIDEO_ELEMENT_TOOLTIP, EPortfolioConstants.CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON,
                false);
        slideShowElement = initChildButton(menuTools, EPortfolioConstants.BUTTON_SLIDESHOW_ELEMENT_LOCATION,
                EPortfolioConstants.SLIDESHOW_ELEMENT_TOOLTIP, EPortfolioConstants.CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON,
                false);
        listElement = initChildButton(menuTools, EPortfolioConstants.BUTTON_LIST_ELEMENT_LOCATION,
                EPortfolioConstants.LIST_ELEMENT_TOOLTIP, EPortfolioConstants.CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON,
                false);

        addPage = initChildButton(pageTools, EPortfolioConstants.BUTTON_ADD_ELEMENT_LOCATION,
                EPortfolioConstants.ADD_PAGE_TOOLTIP, EPortfolioConstants.CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON,
                false);
        removePage = initChildButton(pageTools, EPortfolioConstants.BUTTON_REMOVE_ELEMENT_LOCATION,
                EPortfolioConstants.REMOVE_PAGE_TOOLTIP, EPortfolioConstants.CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON,
                false);
        movePageLeft = initChildButton(pageTools, EPortfolioConstants.BUTTON_MOVELEFT_ELEMENT_LOCATION,
                EPortfolioConstants.MOVELEFT_PAGE_TOOLTIP, EPortfolioConstants.CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON,
                false);
        movePageRight = initChildButton(pageTools, EPortfolioConstants.BUTTON_MOVERIGHT_ELEMENT_LOCATION,
                EPortfolioConstants.MOVERIGHT_PAGE_TOOLTIP, EPortfolioConstants.CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON,
                false);
    }

    private void paintElements() {
        if (ePortfolio.getCurrentPageSelected() == null) {
            return;
        }
        for (PageElement temp : ePortfolio.getCurrentPageSelected().getElements()) {
            Node pane = temp.generateViewHousing();
            pane.addEventHandler(MouseEvent.MOUSE_CLICKED, e -> {
                if (!this.ePortfolio.getCurrentPageSelected().getCurrentElementSelected().equals(temp)) {
                    this.ePortfolio.getCurrentPageSelected().selectElement(temp);
                    paint();
                }
            });
            elements.getChildren().add(pane);
        }
    }

    /**
     * This helps initialize buttons in a toolbar, constructing a custom button
     * with a customly provided icon and tooltip, adding it to the provided
     * toolbar pane, and then returning it.
     */
    private Button initChildButton(
            Pane toolbar,
            String iconFileName,
            String tooltip,
            String cssClass,
            boolean disabled) {
        String imagePath = "file:" + EPortfolioConstants.PATH_ICONS + iconFileName;
        Image buttonImage = new Image(imagePath);
        Button button = new Button();
        button.getStyleClass().add(cssClass);
        button.setDisable(disabled);
        button.setGraphic(new ImageView(buttonImage));
        button.setMinHeight(EPortfolioConstants.MIN_BUTTON_SIZE);
        button.setMinWidth(EPortfolioConstants.MIN_BUTTON_SIZE);
        Tooltip buttonTooltip = new Tooltip(tooltip);
        button.setTooltip(buttonTooltip);
        toolbar.getChildren().add(button);
        return button;
    }

    private void clearElements() {
        pages.getChildren().clear();
        elements.getChildren().clear();
        moveTools.getChildren().clear();
        menuTools.getChildren().clear();
        editor.getChildren().clear();
        pageTools.getChildren().clear();
        pageInformation.getChildren().clear();
    }

}
