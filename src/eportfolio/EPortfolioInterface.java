/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfolio;

import eportfolio.Controller.FileController;
import eportfolio.Elements.PageElement;
import eportfolio.Model.EPortfolioModel;
import eportfolio.View.WebSpace;
import eportfolio.View.WorkSpace;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Screen;
import javafx.stage.Stage;

/**
 *
 * @author James
 */
public class EPortfolioInterface {

    private EPortfolioModel ePortfolio;
    private Button saveButton, saveAsButton, exportButton, exitButton, newButton, loadButton;
    private Button siteView, webView;

    private Scene primaryScene;
    private Stage primaryStage;

    private FileController control;

    private WorkSpace workUI;
    private WebSpace viewUI;

    private FlowPane fileToolbar;
    private FlowPane viewChanger;
    private VBox housing;

    public EPortfolioInterface() {
        ePortfolio = new EPortfolioModel(this);
        control = ePortfolio.getControl();
    }

    public WorkSpace getWorkSpace() {
        return workUI;
    }

    protected void initUI(Stage primaryStage) {
        workUI = generateWorkSpace();
        viewUI = generateWebSpace();
        PageElement.setWorkSpace(workUI);
        this.primaryStage = primeStage(primaryStage);
        this.primaryScene = primeScene();
        primaryStage.setScene(primaryScene);
        primaryStage.show();
    }

    /**
     * Prime the stage to the full size of the window and set the logo.
     *
     * @param primaryStage The primary stage to be returned.
     * @return The primed stage ready for contents.
     */
    private Stage primeStage(Stage primaryStage) {
        Screen screen = Screen.getPrimary();
        Rectangle2D bounds = screen.getVisualBounds();
        primaryStage.setX(bounds.getMinX());
        primaryStage.setY(bounds.getMinY());
        primaryStage.setWidth(bounds.getWidth());
        primaryStage.setHeight(bounds.getHeight());
        primaryStage.setTitle(EPortfolioConstants.APP_TITLE);
        primaryStage.getIcons().add(new Image(EPortfolioConstants.APPLICATION_ICON));
        return primaryStage;
    }

    private Scene primeScene() {
        housing = new VBox();
        fileToolbar = generateFileToolbar();
        viewChanger = generateViewChanger();
        housing.getChildren().add(fileToolbar);
        Scene primaryScene = new Scene(housing);
        primaryScene.getStylesheets().add(EPortfolioConstants.STYLE_SHEET_UI);
        return primaryScene;
    }

    private FlowPane generateFileToolbar() {
        FlowPane pane = new FlowPane();
        newButton = initChildButton(pane, EPortfolioConstants.BUTTON_NEW_LOCATION,
                EPortfolioConstants.NEW_BUTTON_TEXT,
                EPortfolioConstants.CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        loadButton = initChildButton(pane, EPortfolioConstants.BUTTON_LOAD_LOCATION,
                EPortfolioConstants.LOAD_BUTTON_TEXT,
                EPortfolioConstants.CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        saveButton = initChildButton(pane, EPortfolioConstants.BUTTON_SAVE_LOCATION,
                EPortfolioConstants.SAVE_BUTTON_TEXT,
                EPortfolioConstants.CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        saveAsButton = initChildButton(pane, EPortfolioConstants.SAVE_AS_LOCATION,
                EPortfolioConstants.SAVE_AS_TOOLTIP,
                EPortfolioConstants.CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, true);
        exportButton = initChildButton(pane, EPortfolioConstants.BUTTON_EXPORT_LOCATION,
                EPortfolioConstants.EXPORT_BUTTON_TEXT,
                EPortfolioConstants.CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        exitButton = initChildButton(pane, EPortfolioConstants.BUTTON_EXIT_LOCATION,
                EPortfolioConstants.EXIT_BUTTON_TEXT,
                EPortfolioConstants.CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        initializeEventListeners();
        return pane;
    }

    private WorkSpace generateWorkSpace() {
        WorkSpace temp = new WorkSpace(ePortfolio);
        return temp;
    }

    private WebSpace generateWebSpace() {
        WebSpace temp = new WebSpace(ePortfolio);
        return temp;
    }

    private void initializeEventListeners() {
        newButton.setOnAction(e -> {
            control.handleNewSlideShowRequest();
            workUI.paint();
            enableWorkspace();
        });
        saveButton.setOnAction(e -> {
            control.handleSaveSlideShowRequest();
            enableWorkspace();
        });
        loadButton.setOnAction(e -> {
            control.handleLoadSlideShowRequest();
            workUI.paint();
            enableWorkspace();
        });
        saveAsButton.setOnAction(e -> {
            control.handleSaveAsSlideShowRequest();
            enableWorkspace();
        });
        exportButton.setOnAction(e -> {
            control.handleExportRequest();
            workUI.paint();
            enableWorkspace();
        });
        exitButton.setOnAction(e -> {
            control.handleExitRequest();
        });
    }

    public void updateSaveToolbar(boolean disable) {
        saveButton.setDisable(disable);
        saveAsButton.setDisable(disable);
    }

    public void enableSaveToolbar() {
        saveButton.setDisable(false);
        saveAsButton.setDisable(false);
        exportButton.setDisable(false);
    }

    private void enableWorkspace() {
        housing.getChildren().clear();
        housing.getChildren().addAll(fileToolbar, workUI, viewChanger);
    }

    private void enableWebSpace() {
        housing.getChildren().clear();
        viewUI.repaint();
        housing.getChildren().addAll(fileToolbar, viewUI, viewChanger);
    }

    /**
     * This helps initialize buttons in a toolbar, constructing a custom button
     * with a customly provided icon and tooltip, adding it to the provided
     * toolbar pane, and then returning it.
     */
    private Button initChildButton(
            Pane toolbar,
            String iconFileName,
            String tooltip,
            String cssClass,
            boolean disabled) {
        String imagePath = "file:" + EPortfolioConstants.PATH_ICONS + iconFileName;
        Image buttonImage = new Image(imagePath);
        Button button = new Button();
        button.getStyleClass().add(cssClass);
        button.setDisable(disabled);
        button.setGraphic(new ImageView(buttonImage));
        Tooltip buttonTooltip = new Tooltip(tooltip);
        button.setTooltip(buttonTooltip);
        toolbar.getChildren().add(button);
        return button;
    }

    private FlowPane generateViewChanger() {
        FlowPane pane = new FlowPane();
        siteView = initChildButton(pane, EPortfolioConstants.SITEVIEW_ICON_LOCATION,
                EPortfolioConstants.SITEVIEW_ELEMENT_TOOLTIP,
                EPortfolioConstants.CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        siteView.setOnAction(e -> {
            this.enableWorkspace();
        });
        webView = initChildButton(pane, EPortfolioConstants.WEBVIEW_ICON_LOCATION,
                EPortfolioConstants.WEBVIEW_ELEMENT_TOOLTIP,
                EPortfolioConstants.CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        webView.setOnAction(e -> {
            this.enableWebSpace();
        });
        return pane;
    }
}
