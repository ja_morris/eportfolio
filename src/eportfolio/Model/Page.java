/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfolio.Model;

import eportfolio.EPortfolioConstants;
import eportfolio.Elements.PageElement;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author James
 */
public class Page {

    private ObservableList<PageElement> elements;
    private PageElement currentElementSelected;
    private String title, footerText = EPortfolioConstants.DEFAULT_FOOTER;
    private int color, layout, font;

    public PageElement getCurrentElementSelected() {
        return currentElementSelected;
    }

    public String getTitle() {
        return title;
    }

    public String getFooterText() {
        return footerText;
    }

    public void setFooterText(String footerText) {
        this.footerText = footerText;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public int getLayout() {
        return layout;
    }

    public void setLayout(int layout) {
        this.layout = layout;
    }

    public int getFont() {
        return font;
    }

    public void setFont(int font) {
        this.font = font;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Page(String title) {
        this.title = title;
        elements = FXCollections.observableArrayList();
        currentElementSelected = null;
    }

    public ObservableList<PageElement> getElements() {
        return elements;
    }

    public void addElement(PageElement element) {
        currentElementSelected = element;
        elements.add(element);
    }

    public boolean removeElement() {
        if (!elements.isEmpty()) {
            PageElement tempElement = this.getCurrentElementSelected();
            int location = elements.indexOf(tempElement);
            elements.remove(tempElement);
            if (location >= elements.size()) {
                location = elements.size() - 1;
            }
            if (elements.isEmpty()) {
                this.currentElementSelected = null;
            } else {
                this.currentElementSelected = elements.get(location);
            }
            return true;
        }
        return false;
    }

    public boolean moveElementUp() {
        if (!elements.isEmpty()) {
            PageElement temp = this.currentElementSelected;
            int locationOfTemp = elements.indexOf(temp);
            if (locationOfTemp > 0) {
                elements.set(locationOfTemp, elements.get(locationOfTemp - 1));
                elements.set(locationOfTemp - 1, temp);
                return true;
            }
        }
        return false;
    }

    public boolean moveElementDown() {
        if (!elements.isEmpty()) {
            PageElement temp = this.currentElementSelected;
            int locationOfTemp = elements.indexOf(temp);
            if (locationOfTemp < elements.size() - 1) {
                elements.set(locationOfTemp, elements.get(locationOfTemp + 1));
                elements.set(locationOfTemp + 1, temp);
                return true;
            }
        }
        return false;
    }

    public void selectElement(PageElement element) {
        currentElementSelected = element;
    }
}
