package eportfolio.Model;

import eportfolio.Controller.FileController;
import eportfolio.EPortfolio;
import eportfolio.EPortfolioConstants;
import eportfolio.EPortfolioInterface;
import eportfolio.Elements.ImageElement;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author James
 */
public class EPortfolioModel {

    private Page currentPageSelected;
    private ObservableList<Page> pages;
    private EPortfolioInterface ui;
    private FileController control;
    private String studentName = EPortfolioConstants.PAGE_TITLE_DEFAULT;
    private String bannerImage = EPortfolioConstants.PATH_MEDIA + EPortfolioConstants.DEFAULT_IMAGE;
    private String title = EPortfolioConstants.DEFAULT_TITLE;

    public EPortfolioModel(EPortfolioInterface ui) {
        this.ui = ui;
        pages = FXCollections.observableArrayList();
        control = new FileController(this);
        initialStart();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public EPortfolioInterface getUI() {
        return ui;
    }

    public FileController getControl() {
        return control;
    }

    public Page getCurrentPageSelected() {
        return currentPageSelected;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getBannerImage() {
        return bannerImage;
    }

    public void setBannerImage(String bannerImage) {
        this.bannerImage = bannerImage;
    }

    private void initialStart() {
        Page tempPage = new Page(EPortfolioConstants.PAGE_TITLE_DEFAULT);
        addPage(tempPage);
    }

    public void refresh() {
        pages.clear();
        currentPageSelected = null;
        initialStart();
    }

    public void addPage(Page page) {
        currentPageSelected = page;
        pages.add(page);
    }

    public void addNewPage() {
        Page tempPage = new Page(EPortfolioConstants.PAGE_TITLE_DEFAULT);
        addPage(tempPage);
    }

    public boolean removeElement() {
        if (!pages.isEmpty()) {
            Page tempPage = this.getCurrentPageSelected();
            int location = pages.indexOf(tempPage);
            pages.remove(tempPage);
            if (location >= pages.size()) {
                location = pages.size() - 1;
            }
            if (pages.isEmpty()) {
                this.currentPageSelected = null;
            } else {
                this.currentPageSelected = pages.get(location);
            }
            return true;
        }
        return false;
    }

    public boolean movePageUp() {
        if (!pages.isEmpty()) {
            Page temp = this.currentPageSelected;
            int locationOfTemp = pages.indexOf(temp);
            if (locationOfTemp > 0) {
                pages.set(locationOfTemp, pages.get(locationOfTemp - 1));
                pages.set(locationOfTemp - 1, temp);
                return true;
            }
        }
        return false;
    }

    public boolean movePageDown() {
        if (!pages.isEmpty()) {
            Page temp = this.currentPageSelected;
            int locationOfTemp = pages.indexOf(temp);
            if (locationOfTemp < pages.size() - 1) {
                pages.set(locationOfTemp, pages.get(locationOfTemp + 1));
                pages.set(locationOfTemp + 1, temp);
                return true;
            }
        }
        return false;
    }

    public void selectElement(Page page) {
        currentPageSelected = page;
    }

    public int getPageCount() {
        return pages.size();
    }

    public ObservableList<Page> getPages() {
        return pages;
    }

    public void updatePortfolio(String title, String bannerImage, String studentName, ObservableList<Page> pages) {
        this.title = title;
        this.bannerImage = bannerImage;
        this.studentName = studentName;
        this.pages.clear();
        for (Page temp : pages) {
            this.pages.add(temp);
        }
        this.currentPageSelected = pages.get(0);
    }
}
