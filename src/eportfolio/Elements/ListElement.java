/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfolio.Elements;

import FXMLControllers.ListController;
import FXMLControllers.VideoController;
import eportfolio.EPortfolioConstants;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 *
 * @author James
 */
public class ListElement extends PageElement {

    private String title = EPortfolioConstants.DEFAULT_TITLE;
    private List<String> list;
    private Stage viewStage;
    private ArrayList<String> tempList;
    private boolean altered;

    public ListElement(String title, ArrayList<String> list) {
        this.title = title;
        this.list = list;
        tempList = new ArrayList();
    }

    public ListElement() {
        this(EPortfolioConstants.DEFAULT_LIST_ELEMENT_TITLE, new ArrayList());
    }

    public ListElement(ArrayList<String> list) {
        this(EPortfolioConstants.DEFAULT_LIST_ELEMENT_TITLE, list);
    }

    public ListElement(String title) {
        this(title, new ArrayList());
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public List<String> getList() {
        return list;
    }

    @Override
    public void generateWorkHousing() {
        viewStage = new Stage();
        viewStage.setTitle(EPortfolioConstants.TEXT_ELEMENT_TITLE);
        viewStage.setResizable(false);

        AnchorPane pane;
        FXMLLoader loader;
        ListController headController;
        try {
            pane = (loader = new FXMLLoader(super.retrieveFXMLURL(ElementDistinguisher.List))).load();
            headController = loader.getController();
        } catch (IOException ex) {
            Logger.getLogger(HeaderElement.class.getName()).log(Level.SEVERE, null, ex);
            return;
        }
        headController.initializeVariables(this.getTitle(), this.getList());
        headController.getBtnCancel().setOnAction(e -> {
            viewStage.close();
        });
        headController.getBtnOk().setOnAction(e -> {
            parseData(headController.getList(), headController.getTitle());
            PageElement.getWorkSpace().paint();
            viewStage.close();
        });
        viewStage.setScene(new Scene(pane));
        viewStage.showAndWait();
    }

    @Override
    public Node generateViewHousing() {
        VBox housing = new VBox();
        super.primeElement(housing);

        Text text = new Text(this.title);
        Text blankText = new Text(EPortfolioConstants.EMPTY_LIST_COMPONENT);
        housing.getChildren().add(text);
        if (list.isEmpty()) {
            housing.getChildren().add(blankText);
        } else {
            housing.getChildren().add(new Text("\n"));
            for (String temp : list) {
                housing.getChildren().add(new Text(temp));
            }
        }
        return housing;
    }

    private void parseData(List<String> updateList, String title) {
        this.title = title;
        list.clear();
        for (String temp : updateList) {
            list.add(temp);
        }
    }
}
