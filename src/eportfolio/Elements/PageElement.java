package eportfolio.Elements;

import eportfolio.View.WorkSpace;
import java.net.URL;
import javafx.geometry.Rectangle2D;
import javafx.scene.Node;
import javafx.scene.layout.Pane;
import javafx.stage.Screen;

/**
 *
 * @author James
 */
public abstract class PageElement {

    private static WorkSpace workspace;

    public static void setWorkSpace(WorkSpace work) {
        PageElement.workspace = work;
    }

    public static WorkSpace getWorkSpace() {
        return workspace;
    }

    protected void primeElement(Pane pane) {
        Screen screen = Screen.getPrimary();
        Rectangle2D bounds = screen.getVisualBounds();
        pane.setMinWidth(bounds.getWidth() / 2);
        pane.setMinHeight(bounds.getHeight() / 8);
        if (PageElement.getWorkSpace().getEPortfolio().getCurrentPageSelected().getCurrentElementSelected().equals(this)) {
            primeSelected(pane);
        } else {
            primeUnselect(pane);
        }
    }

    protected URL retrieveFXMLURL(ElementDistinguisher elementDistinguisher) {
        URL temp = null;
        if (elementDistinguisher == ElementDistinguisher.Header) {
            temp = getClass().getResource("/FXMLControllers/Header.fxml");
        } else if (elementDistinguisher == ElementDistinguisher.Image) {
            temp = getClass().getResource("/FXMLControllers/Image.fxml");
        } else if (elementDistinguisher == ElementDistinguisher.Video) {
            temp = getClass().getResource("/FXMLControllers/Video.fxml");
        } else if (elementDistinguisher == ElementDistinguisher.SlideShow) {
            temp = getClass().getResource("/FXMLControllers/SlideShow.fxml");
        } else if (elementDistinguisher == ElementDistinguisher.List) {
            temp = getClass().getResource("/FXMLControllers/List.fxml");
        } else if (elementDistinguisher == ElementDistinguisher.Paragraph) {
            temp = getClass().getResource("/FXMLControllers/Text.fxml");
        }
        return temp;
    }

    private void primeSelected(Node selectedElement) {
        selectedElement.setStyle("-fx-border-color: blue;\n"
                + "-fx-border-insets: 5;\n"
                + "-fx-border-width: 3;\n"
                + "-fx-border-style: dashed;\n");
    }

    private void primeUnselect(Node selectedElement) {
        selectedElement.setStyle("-fx-border-color: black;\n"
                + "-fx-border-insets: 5;\n"
                + "-fx-border-width: 3;\n"
                + "-fx-border-style: solid;\n");
    }

    public abstract void generateWorkHousing();

    public abstract Node generateViewHousing();
}
