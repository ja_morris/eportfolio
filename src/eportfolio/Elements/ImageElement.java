package eportfolio.Elements;

import FXMLControllers.ImageController;
import FXMLControllers.TextController;
import eportfolio.Controller.ImageSelectionController;
import eportfolio.EPortfolioConstants;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 *
 * @author James
 */
public class ImageElement extends PageElement {

    private int width, height;
    private String caption = EPortfolioConstants.MEDIA_ELEMENT_DEFAULT,
            sourceFile = EPortfolioConstants.PATH_MEDIA + EPortfolioConstants.DEFAULT_IMAGE;
    private int floatLocation; //0 is left, 1 is center, 2 is right;
    private Stage viewStage;
    private String tempSourceFile;

    public void setTempSourceFile(String s) {
        this.tempSourceFile = s;
    }

    public int getFloatLocation() {
        return floatLocation;
    }

    public void setFloatLocation(int floatLocation) {
        this.floatLocation = floatLocation;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public String getSourceFile() {
        return sourceFile;
    }

    public void setSourceFile(String sourceFile) {
        this.sourceFile = sourceFile;
    }

    @Override
    public Node generateViewHousing() {
        Image image = new Image("file:" + this.getSourceFile());
        ImageView imageView = new ImageView(image);
        imageView.maxHeight(this.getHeight());
        imageView.maxWidth(this.getWidth());

        Text text = new Text();
        text.setText(this.getCaption());

        FlowPane pane = new FlowPane();
        pane.getChildren().add(imageView);
        pane.getChildren().add(text);
        super.primeElement(pane);
        return pane;
    }

    private void parseData(int height, int width, int floatLocation, String pathLocation, String caption) {
        this.height = height;
        this.width = width;
        this.sourceFile = pathLocation;
        this.caption = caption;
        this.floatLocation = floatLocation;
    }

    @Override
    public void generateWorkHousing() {
        viewStage = new Stage();
        viewStage.setTitle(EPortfolioConstants.TEXT_ELEMENT_TITLE);
        viewStage.setResizable(false);

        AnchorPane pane;
        FXMLLoader loader;
        ImageController headController;
        try {
            pane = (loader = new FXMLLoader(super.retrieveFXMLURL(ElementDistinguisher.Image))).load();
            headController = loader.getController();
        } catch (IOException ex) {
            Logger.getLogger(HeaderElement.class.getName()).log(Level.SEVERE, null, ex);
            return;
        }
        headController.initializeVariables(this.getSourceFile(), this.getCaption(), this.getFloatLocation(), this.getHeight(), this.getWidth());
        headController.getBtnCancel().setOnAction(e -> {
            viewStage.close();
        });
        headController.getBtnOk().setOnAction(e -> {
            parseData(headController.getHeight(), headController.getWidth(),
                    headController.getFloatLocation(), headController.getLocation(),
                    headController.getCaption());
            PageElement.getWorkSpace().paint();
            viewStage.close();
        });
        viewStage.setScene(new Scene(pane));
        viewStage.showAndWait();

    }

}
