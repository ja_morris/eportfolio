/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfolio.Elements;

import FXMLControllers.SlideShowController;
import FXMLControllers.VideoController;
import eportfolio.EPortfolioConstants;
import eportfolio.Elements.SlideElements.Slide;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Screen;
import javafx.stage.Stage;

/**
 *
 * @author James
 */
public class SlideShowElement extends PageElement {

    private ArrayList<Slide> slides;
    private Stage viewStage;

    public SlideShowElement() {
        slides = new ArrayList();
    }

    public ArrayList<Slide> getSlides() {
        return slides;
    }

    @Override
    public void generateWorkHousing() {
        viewStage = new Stage();
        viewStage.setTitle(EPortfolioConstants.TEXT_ELEMENT_TITLE);
        viewStage.setResizable(false);
        viewStage.setTitle(EPortfolioConstants.SLIDESHOW_ELEMENT_TITLE);

        AnchorPane pane;
        FXMLLoader loader;
        SlideShowController headController;
        try {
            pane = (loader = new FXMLLoader(super.retrieveFXMLURL(ElementDistinguisher.SlideShow))).load();
            headController = loader.getController();
        } catch (IOException ex) {
            Logger.getLogger(HeaderElement.class.getName()).log(Level.SEVERE, null, ex);
            return;
        }
        headController.initializeVariables(this.getSlides());
        headController.getBtnCancel().setOnAction(e -> {
            viewStage.close();
        });
        headController.getBtnOk().setOnAction(e -> {
            this.slides = headController.getSlides();
            PageElement.getWorkSpace().paint();
            viewStage.close();
        });
        viewStage.setScene(new Scene(pane));
        viewStage.showAndWait();
    }

    @Override
    public Node generateViewHousing() {
        VBox pane = new VBox();
        for (int i = 0; i < slides.size(); i++) {
            pane.getChildren().add(slides.get(i).generateViewHousing());
        }
        super.primeElement(pane);
        return pane;
    }
}
