package eportfolio.Elements;

import FXMLControllers.HeaderController;
import FXMLControllers.TextController;
import eportfolio.EPortfolioConstants;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.geometry.Rectangle2D;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextArea;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Screen;
import javafx.stage.Stage;

/**
 *
 * @author James
 */
public class TextElement extends PageElement {

    private String message;
    private Stage viewStage;
    private int font;

    public TextElement() {
        this(EPortfolioConstants.TEXT_ELEMENT_DEFAULT);
    }

    public int getFont() {
        return font;
    }

    public void setFont(int font) {
        this.font = font;
    }

    public TextElement(String message) {
        this.message = message;
    }

    public TextElement(String message, int font) {

    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    private void parseData(String parse, int font) {
        this.message = parse;
        this.font = font;
    }

    @Override
    public Node generateViewHousing() {
        Text text = new Text();
        text.setText(this.getMessage());
        VBox pane = new VBox();
        pane.getChildren().add(text);
        pane.getChildren().add(new Label("Font Selected" + (this.font + 1)));
        super.primeElement(pane);
        return pane;
    }

    @Override
    public void generateWorkHousing() {
        viewStage = new Stage();
        viewStage.setTitle(EPortfolioConstants.TEXT_ELEMENT_TITLE);
        viewStage.setResizable(false);

        AnchorPane pane;
        FXMLLoader loader;
        TextController headController;
        try {
            pane = (loader = new FXMLLoader(super.retrieveFXMLURL(ElementDistinguisher.Paragraph))).load();
            headController = loader.getController();
        } catch (IOException ex) {
            Logger.getLogger(HeaderElement.class.getName()).log(Level.SEVERE, null, ex);
            return;
        }
        headController.initializeVariables(new Object[]{this.message, this.font});
        headController.getBtnCancel().setOnAction(e -> {
            viewStage.close();
        });
        headController.getBtnOk().setOnAction(e -> {
            parseData(headController.getMessage(), headController.getFontLocation());
            PageElement.getWorkSpace().paint();
            viewStage.close();
        });
        viewStage.setScene(new Scene(pane));
        viewStage.showAndWait();
    }
}
