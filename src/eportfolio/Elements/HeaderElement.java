/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfolio.Elements;

import FXMLControllers.HeaderController;
import eportfolio.EPortfolioConstants;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextArea;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 *
 * @author James
 */
public class HeaderElement extends PageElement {

    private String message;
    private Stage viewStage;

    public HeaderElement(String message) {
        this.message = message;
    }

    public HeaderElement() {
        this(EPortfolioConstants.TEXT_ELEMENT_DEFAULT);
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public void generateWorkHousing() {
        viewStage = new Stage();
        viewStage.setTitle(EPortfolioConstants.TEXT_ELEMENT_TITLE);
        viewStage.setResizable(false);
        AnchorPane pane;
        FXMLLoader loader;
        HeaderController headController;
        try {
            pane = (loader = new FXMLLoader(super.retrieveFXMLURL(ElementDistinguisher.Header))).load();
            headController = loader.getController();
        } catch (IOException ex) {
            Logger.getLogger(HeaderElement.class.getName()).log(Level.SEVERE, null, ex);
            return;
        }
        headController.initializeVariables(new Object[]{message});
        headController.getBtnCancel().setOnAction(e -> {
            viewStage.close();
        });
        headController.getBtnOk().setOnAction(e -> {
            parseMessage(headController.getTextArea().getText());
            PageElement.getWorkSpace().paint();
            viewStage.close();
        });
        viewStage.setScene(new Scene(pane));
        viewStage.showAndWait();
    }

    @Override
    public Node generateViewHousing() {
        Text text = new Text();
        text.setText(this.getMessage());
        text.getStyleClass().add("header");
        VBox pane = new VBox();
        pane.getChildren().add(text);
        super.primeElement(pane);
        return pane;
    }

    private void parseMessage(String text) {
        this.message = text;
    }

}
