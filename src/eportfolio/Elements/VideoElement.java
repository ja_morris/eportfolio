package eportfolio.Elements;

import FXMLControllers.TextController;
import FXMLControllers.VideoController;
import eportfolio.Controller.ImageSelectionController;
import eportfolio.Controller.VideoSelectionController;
import eportfolio.EPortfolioConstants;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 *
 * @author James
 */
public class VideoElement extends PageElement {

    private int width, height;
    private String caption = EPortfolioConstants.MEDIA_ELEMENT_DEFAULT,
            sourceFile = EPortfolioConstants.DEFAULT_VIDEO;
    private Stage viewStage;
    private String tempSourceFile;

    public void setTempSourceFile(String s) {
        this.tempSourceFile = s;
    }

    public String getTempSourceFile() {
        return this.tempSourceFile;
    }

    public Stage getViewStage() {
        return viewStage;
    }

    public void setViewStage(Stage viewStage) {
        this.viewStage = viewStage;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public String getSourceFile() {
        return sourceFile;
    }

    public void setSourceFile(String sourceFile) {
        this.sourceFile = sourceFile;
    }

    @Override
    public Node generateViewHousing() {
        // Create the player and set to play automatically.
        MediaPlayer mediaPlayer = new MediaPlayer(this.generateMedia());
        MediaView view = new MediaView(mediaPlayer);
        view.setOnMouseEntered(e -> {
            if (PageElement.getWorkSpace().getEPortfolio().getCurrentPageSelected().getCurrentElementSelected().equals(this)) {
                view.getMediaPlayer().play();
            }
        });
        view.setOnMouseExited(e -> {
            view.getMediaPlayer().pause();
        });

        Text text = new Text();
        text.setText(this.getCaption());

        FlowPane pane = new FlowPane();
        pane.getChildren().addAll(view, text);
        super.primeElement(pane);
        return pane;
    }

    private void parseData(int height, int width, String pathLocation, String caption) {
        this.height = height;
        this.width = width;
        this.sourceFile = pathLocation;
        this.caption = caption;
    }

    @Override
    public void generateWorkHousing() {
        viewStage = new Stage();
        viewStage.setTitle(EPortfolioConstants.TEXT_ELEMENT_TITLE);
        viewStage.setResizable(false);
        tempSourceFile = "";

        AnchorPane pane;
        FXMLLoader loader;
        VideoController headController;
        try {
            pane = (loader = new FXMLLoader(super.retrieveFXMLURL(ElementDistinguisher.Video))).load();
            headController = loader.getController();
        } catch (IOException ex) {
            Logger.getLogger(HeaderElement.class.getName()).log(Level.SEVERE, null, ex);
            return;
        }
        headController.initializeVariables(this.getSourceFile(), this.getCaption(), this.getHeight(), this.getWidth());
        headController.getBtnCancel().setOnAction(e -> {
            viewStage.close();
        });
        headController.getBtnOk().setOnAction(e -> {
            parseData(headController.getHeight(), headController.getWidth(),
                    headController.getLocation(), headController.getCaption());
            PageElement.getWorkSpace().paint();
            viewStage.close();
        });
        viewStage.setScene(new Scene(pane));
        viewStage.showAndWait();
    }

    private Media generateMedia() {
        if (this.sourceFile.toLowerCase().contains("http")) {
            return new Media(this.sourceFile);
        } else {
            return new Media(new File(this.getSourceFile()).toURI().toString());
        }
    }

}
