/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfolio.Elements.SlideElements;

import FXMLControllers.SlideController;
import FXMLControllers.VideoController;
import eportfolio.Controller.ImageSelectionController;
import eportfolio.Controller.SlideSelectionController;
import eportfolio.EPortfolioConstants;
import eportfolio.Elements.ElementDistinguisher;
import eportfolio.Elements.HeaderElement;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

/**
 *
 * @author James
 */
public class Slide {

    private int height, width;
    private String fileLocation = EPortfolioConstants.PATH_MEDIA + EPortfolioConstants.DEFAULT_IMAGE;
    private String caption = EPortfolioConstants.TEXT_ELEMENT_DEFAULT;

    public Slide() {
    }

    public Slide(int height, int width, String fileLocation, String caption) {
        this.height = height;
        this.width = width;
        this.fileLocation = fileLocation;
        this.caption = caption;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public String getFileLocation() {
        return fileLocation;
    }

    public void setFileLocation(String fileLocation) {
        this.fileLocation = fileLocation;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public SlideController generateWorkHousing() {
        AnchorPane pane;
        FXMLLoader loader;
        SlideController headController;
        try {
            pane = (loader = new FXMLLoader(getClass().getResource("/FXMLControllers/Slide.fxml"))).load();
            headController = loader.getController();
        } catch (IOException ex) {
            Logger.getLogger(HeaderElement.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        headController.initializeVariables(this, pane);
        return headController;
    }

    public HBox generateViewHousing() {
        return new HBox(new ImageView(new Image("file:" + this.getFileLocation())), new Text(this.getCaption()));
    }

}
