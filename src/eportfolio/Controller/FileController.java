package eportfolio.Controller;

import static eportfolio.Controller.EPortfolioFileManager.updateWebPortfolio;
import eportfolio.EPortfolioConstants;
import eportfolio.Model.EPortfolioModel;
import java.io.File;
import java.io.IOException;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

/**
 *
 * @author James
 */
public class FileController {

    public static String ALERT_TITLE = "Confirm Save";
    public static String ALERT_HEADER_TEXT = "It seems you have made changes to your current project.";
    public static String ALERT_CONTENT_TEXT = "Are you sure you would like to save?";
    public static String ALERT_CONFIRM = "Confirm";
    public static String ALERT_SAVE_STATUS = "Data was not saved.";
    public static String ERRORS = "Error Detected";
    public static String SAVE_PORTFOLIO_ERROR = "could not be saved";
    public static String EXIT_PORTFOLIO_ERROR = "could not be exited";
    public static String LOAD_PORTFOLIO_ERROR = "could not be loaded";
    public static String NEW_PORTFOLIO = "A new portfolio has been generated";
    public static String NEW_PORTFOLIO_ERROR = "new portfolio could not be created";

    private EPortfolioModel ePortfolio;

    // WE WANT TO KEEP TRACK OF WHEN SOMETHING HAS NOT BEEN SAVED
    private boolean saved;
    private Alert alert;
    private Alert alertConfirm;
    private ButtonType buttonTypeOK, buttonTypeNO, buttonTypeCANCEL;

    public FileController(EPortfolioModel ePortfolio) {
        this.ePortfolio = ePortfolio;
        saved = true;
        initAlert();
    }

    private void initAlert() {
        alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle(ALERT_TITLE);
        alert.setHeaderText(ALERT_HEADER_TEXT);
        alert.setContentText(ALERT_CONTENT_TEXT);
        buttonTypeOK = new ButtonType("OK", ButtonBar.ButtonData.YES);
        buttonTypeNO = new ButtonType("NO", ButtonBar.ButtonData.NO);
        buttonTypeCANCEL = new ButtonType("Cancel", ButtonBar.ButtonData.CANCEL_CLOSE);
        alert.getButtonTypes().setAll(buttonTypeOK, buttonTypeNO, buttonTypeCANCEL);

        alertConfirm = new Alert(Alert.AlertType.INFORMATION);
        alertConfirm.setTitle(ALERT_CONFIRM);
        alertConfirm.setHeaderText(null);
        alertConfirm.setContentText(ALERT_SAVE_STATUS);
        alertConfirm.getButtonTypes().setAll(buttonTypeOK);

    }

    /**
     * This method will save the current slideshow to a file. Note that we
     * already know the name of the file, so we won't need to prompt the user.
     */
    public boolean handleSaveSlideShowRequest() {
        try {
            // GET THE SLIDE SHOW TO SAVE

            if (!saved) {
                Optional<ButtonType> result = alert.showAndWait();
                if (result.get() == buttonTypeOK) {
                    // SAVE IT TO A FILE
                    EPortfolioFileManager.saveSlideShow(EPortfolioConstants.PATH_SAVED + "/", ePortfolio);

                    // MARK IT AS SAVED
                    saved = true;

                    // AND REFRESH THE GUI, WHICH WILL ENABLE AND DISABLE
                    // THE APPROPRIATE CONTROLS
                    ePortfolio.getUI().updateSaveToolbar(saved);
                } else {
                    alertConfirm.showAndWait();
                }
            }
            return true;
        } catch (IOException ioe) {
            handleError(ALERT_TITLE, ERRORS, SAVE_PORTFOLIO_ERROR);
            return false;
        }
    }

    public boolean handleSaveAsSlideShowRequest() {
        try {
            // GET THE SLIDE SHOW TO SAVE

            if (!saved) {
                Optional<ButtonType> result = alert.showAndWait();
                if (result.get() == buttonTypeOK) {

                    DirectoryChooser imageFileChooser = new DirectoryChooser();

                    // SET THE STARTING DIRECTORY
                    imageFileChooser.setInitialDirectory(new File(EPortfolioConstants.PATH_SAVED));

                    // LET'S OPEN THE FILE CHOOSER
                    File file = imageFileChooser.showDialog(new Stage());
                    if (file != null) {
                        // SAVE IT TO A FILE
                        EPortfolioFileManager.saveSlideShow(file.getAbsolutePath(), ePortfolio);

                        // MARK IT AS SAVED
                        saved = true;

                        // AND REFRESH THE GUI, WHICH WILL ENABLE AND DISABLE
                        // THE APPROPRIATE CONTROLS
                        ePortfolio.getUI().updateSaveToolbar(saved);
                    } else {
                        // @todo provide error message for no files selected
                    }
                } else {
                    alertConfirm.showAndWait();
                }
            }
            return true;
        } catch (IOException ioe) {
            handleError(ALERT_TITLE, ERRORS, SAVE_PORTFOLIO_ERROR);
            return false;
        }
    }

    /**
     * This method lets the user open a slideshow saved to a file. It will also
     * make sure data for the current slideshow is not lost.
     */
    public void handleLoadSlideShowRequest() {
        try {
            // WE MAY HAVE TO SAVE CURRENT WORK
            boolean continueToOpen = true;
            if (!saved) {
                // THE USER CAN OPT OUT HERE WITH A CANCEL
                continueToOpen = promptToSave();
            }

            // IF THE USER REALLY WANTS TO OPEN A POSE
            if (continueToOpen) {
                // GO AHEAD AND PROCEED MAKING A NEW POSE
                promptToOpen();
            }
        } catch (IOException ioe) {
            handleError(ALERT_TITLE, ERRORS, LOAD_PORTFOLIO_ERROR);
        }
    }

    /**
     * This method starts the process of editing a new slide show. If a pose is
     * already being edited, it will prompt the user to save it first.
     */
    public void handleNewSlideShowRequest() {
        try {
            // WE MAY HAVE TO SAVE CURRENT WORK
            boolean continueToMakeNew = true;
            if (!saved) {
                // THE USER CAN OPT OUT HERE WITH A CANCEL
                continueToMakeNew = promptToSave();
            }

            // IF THE USER REALLY WANTS TO MAKE A NEW COURSE
            if (continueToMakeNew) {
                // RESET THE DATA, WHICH SHOULD TRIGGER A RESET OF THE UI
                ePortfolio.refresh();
                saved = false;

                // REFRESH THE GUI, WHICH WILL ENABLE AND DISABLE
                // THE APPROPRIATE CONTROLS
                ePortfolio.getUI().updateSaveToolbar(saved);

                // TELL THE USER THE SLIDE SHOW HAS BEEN CREATED
                // GET THE FEEDBACK TEXT
                Alert alertConfirm = new Alert(Alert.AlertType.INFORMATION);
                alertConfirm.setTitle("");
                alertConfirm.setHeaderText(NEW_PORTFOLIO);
                alertConfirm.setContentText(null);
                alertConfirm.getButtonTypes().setAll(buttonTypeOK);
                alertConfirm.showAndWait();
            }
        } catch (IOException ioe) {
            handleError(ALERT_TITLE, ERRORS, NEW_PORTFOLIO_ERROR);
        }
    }

    /**
     * This method will exit the application, making sure the user doesn't lose
     * any data first.
     */
    public void handleExitRequest() {
        try {
            // WE MAY HAVE TO SAVE CURRENT WORK
            boolean continueToExit = true;
            if (!saved) {
                // THE USER CAN OPT OUT HERE
                continueToExit = promptToSave();
            }

            // IF THE USER REALLY WANTS TO EXIT THE APP
            if (continueToExit) {
                // EXIT THE APPLICATION
                System.exit(0);
            }
        } catch (IOException ioe) {
            handleError(ALERT_TITLE, ERRORS, EXIT_PORTFOLIO_ERROR);
        }
    }

    /**
     * This helper method verifies that the user really wants to save their
     * unsaved work, which they might not want to do. Note that it could be used
     * in multiple contexts before doing other actions, like creating a new
     * pose, or opening another pose, or exiting. Note that the user will be
     * presented with 3 options: YES, NO, and CANCEL. YES means the user wants
     * to save their work and continue the other action (we return true to
     * denote this), NO means don't save the work but continue with the other
     * action (true is returned), CANCEL means don't save the work and don't
     * continue with the other action (false is retuned).
     *
     * @return true if the user presses the YES option to save, true if the user
     * presses the NO option to not save, false if the user presses the CANCEL
     * option to not continue.
     */
    private boolean promptToSave() throws IOException {
        // PROMPT THE USER TO SAVE UNSAVED WORK

        boolean saveWork = false;
        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == buttonTypeOK) {
            saveWork = true;
        } else if (result.get() == buttonTypeNO) {
            saveWork = false;
        } else {
            return false;
        }

        // IF THE USER SAID YES, THEN SAVE BEFORE MOVING ON
        if (saveWork) {
            EPortfolioFileManager.saveSlideShow(EPortfolioConstants.PATH_SAVED + "/", ePortfolio);
            saved = true;
        } // IF THE USER SAID CANCEL, THEN WE'LL TELL WHOEVER
        // CALLED THIS THAT THE USER IS NOT INTERESTED ANYMORE
        else if (!true) {
            return false;
        }

        // IF THE USER SAID NO, WE JUST GO ON WITHOUT SAVING
        // BUT FOR BOTH YES AND NO WE DO WHATEVER THE USER
        // HAD IN MIND IN THE FIRST PLACE
        return true;
    }

    /**
     * This helper method asks the user for a file to open. The user-selected
     * file is then loaded and the GUI updated. Note that if the user cancels
     * the open process, nothing is done. If an error occurs loading the file, a
     * message is displayed, but nothing changes.
     */
    private void promptToOpen() {
        // AND NOW ASK THE USER FOR THE COURSE TO OPEN
        FileChooser slideShowFileChooser = new FileChooser();
        slideShowFileChooser.setInitialDirectory(new File(EPortfolioConstants.PATH_SAVED));
        File selectedFile = slideShowFileChooser.showOpenDialog(new Stage());

        // ONLY OPEN A NEW FILE IF THE USER SAYS OK
        if (selectedFile != null) {
            try {
                EPortfolioFileManager.loadSlideShow(this.ePortfolio, selectedFile.getAbsolutePath());
                ePortfolio.getUI().getWorkSpace().paint();
                saved = true;
                ePortfolio.getUI().updateSaveToolbar(saved);
            } catch (Exception e) {
                handleError(ALERT_TITLE, ERRORS, LOAD_PORTFOLIO_ERROR);
            }
        }
    }

    /**
     * This mutator method marks the file as not saved, which means that when
     * the user wants to do a file-type operation, we should prompt the user to
     * save current work first. Note that this method should be called any time
     * the pose is changed in some way.
     */
    public void markFileAsNotSaved() {
        saved = false;
        ePortfolio.getUI().updateSaveToolbar(saved);
    }

    /**
     * Accessor method for checking to see if the current pose has been saved
     * since it was last editing. If the current file matches the pose data,
     * we'll return true, otherwise false.
     *
     * @return true if the current pose is saved to the file, false otherwise.
     */
    public boolean isSaved() {
        return saved;
    }

    private void handleError(String title, String header, String context) {
        Alert temp;
        temp = new Alert(Alert.AlertType.INFORMATION);
        temp.setTitle(title);
        temp.setHeaderText(header);
        temp.setContentText(context);
        temp.getButtonTypes().setAll(buttonTypeOK);
    }

    public void handleExportRequest() {
        try {
            EPortfolioFileManager.ExportPortfolio(ePortfolio);
        } catch (IOException ex) {
            Logger.getLogger(FileController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
