package eportfolio.Controller;

import eportfolio.EPortfolioConstants;
import eportfolio.Elements.ImageElement;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.stage.FileChooser;

/**
 * This controller provides a controller for when the user chooses to select an
 * image for the slide show.
 *
 * @author McKilla Gorilla & _____________
 */
public class GeneralSelectionController {

    /**
     * Default contstructor doesn't need to initialize anything
     */
    public GeneralSelectionController() {
    }

    /**
     * This function provides the response to the user's request to select an
     * image.
     *
     * @param element - Slide for which the user is selecting an image.
     *
     * @param view The user interface control group where the image will appear
     * after selection.
     */
    public String processSelectImage(String element) {
        FileChooser imageFileChooser = new FileChooser();

        // SET THE STARTING DIRECTORY
        imageFileChooser.setInitialDirectory(new File(EPortfolioConstants.PATH_MEDIA));

        // LET'S ONLY SEE IMAGE FILES
        FileChooser.ExtensionFilter jpgFilter = new FileChooser.ExtensionFilter("JPG files (*.jpg)", "*.JPG");
        FileChooser.ExtensionFilter pngFilter = new FileChooser.ExtensionFilter("PNG files (*.png)", "*.PNG");
        FileChooser.ExtensionFilter gifFilter = new FileChooser.ExtensionFilter("GIF files (*.gif)", "*.GIF");
        imageFileChooser.getExtensionFilters().addAll(jpgFilter, pngFilter, gifFilter);

        // LET'S OPEN THE FILE CHOOSER
        File file = imageFileChooser.showOpenDialog(null);
        if (file != null) {
            String path = file.getPath().substring(0, file.getPath().indexOf(file.getName()));
            String fileName = file.getName();
            element = (path + fileName);
            try {
                if (!new File(EPortfolioConstants.PATH_MEDIA + file.getName()).exists()) {
                    Files.copy(file.toPath(), new File(EPortfolioConstants.PATH_MEDIA + file.getName()).toPath());
                }
            } catch (IOException ex) {
                Logger.getLogger(GeneralSelectionController.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            // @todo provide error message for no files selected
        }
        return element;
    }

}
