package eportfolio.Controller;

import com.google.gson.Gson;
import eportfolio.EPortfolioConstants;
import eportfolio.Elements.HeaderElement;
import eportfolio.Elements.ImageElement;
import eportfolio.Elements.ListElement;
import eportfolio.Elements.PageElement;
import eportfolio.Elements.SlideElements.Slide;

import eportfolio.Elements.SlideShowElement;
import eportfolio.Elements.TextElement;
import eportfolio.Elements.VideoElement;
import eportfolio.Model.EPortfolioModel;
import eportfolio.Model.Page;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonReader;
import javax.json.JsonValue;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;
import javax.json.stream.JsonGeneratorFactory;

/**
 * This class uses the JSON standard to read and write slideshow data files.
 *
 * @author McKilla Gorilla & _____________
 */
public class EPortfolioFileManager {
    // JSON FILE READING AND WRITING CONSTANTS

    /**
     * This method saves all the data associated with a slide show to a JSON
     * file.
     *
     * @param ePortfolioToSave The course whose data we are saving.
     *
     * @throws IOException Thrown when there are issues writing to the JSON
     * file.
     */
    public static void saveSlideShow(String directory, EPortfolioModel ePortfolioToSave) throws IOException {
        // BUILD THE FILE PATH
        String portfolioTitle = ePortfolioToSave.getTitle();
        String jsonFilePath = directory + SLASH + portfolioTitle + JSON_EXT;

        File file = new File(jsonFilePath);
        OutputStream os = new FileOutputStream(file);
        if (!file.exists()) {
            file.createNewFile();
        }
        // INIT THE WRITER

        // BUILD THE SLIDES ARRAY
        try (JsonWriter writer = Json.createWriter(os)) {
            //         AND SAVE EVERYTHING AT ONCE
            writer.writeObject(convertJson(ePortfolioToSave));
        }

        prettyPrint(convertJson(ePortfolioToSave));
    }

    public static JsonObject convertJson(EPortfolioModel ePortfolioToSave) {
        JsonObject courseJsonObject;
        JsonArray pagesJsonArray = makePagesArray(ePortfolioToSave.getPages());
        // NOW BUILD THE COURSE USING EVERYTHING WE'VE ALREADY MADE
        courseJsonObject = Json.createObjectBuilder()
                .add(JSON_NAME, ePortfolioToSave.getStudentName())
                .add(JSON_BANNER, new File("file:" + ePortfolioToSave.getBannerImage()).getName())
                .add(JSON_TITLE, ePortfolioToSave.getTitle())
                .add(JSON_PAGES, pagesJsonArray)
                .build();
        return courseJsonObject;
    }

    private static void prettyPrint(JsonObject courseJsonObject) {
        StringWriter write = new StringWriter();
        Map<String, Object> properties = new HashMap<>(1);
        properties.put(JsonGenerator.PRETTY_PRINTING, true);

        JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
        JsonWriter jsonWriter = writerFactory.createWriter(write);

        jsonWriter.writeObject(courseJsonObject);
        jsonWriter.close();
        System.out.println(write.toString());
    }

    public static void ExportPortfolio(EPortfolioModel toExport) throws IOException {
        DirectoryChooser imageFileChooser = new DirectoryChooser();

        // SET THE STARTING DIRECTORY
        imageFileChooser.setInitialDirectory(new File(EPortfolioConstants.PATH_SITES));

        // LET'S OPEN THE FILE CHOOSER
        File file = imageFileChooser.showDialog(new Stage());
        if (file != null) {
            updateWebPortfolio(file.getAbsolutePath(), toExport);
        } else {
            // @todo provide error message for no files selected
        }
    }

    public static URL updateWebPortfolio(String directory, EPortfolioModel update) throws IOException {
        String location = directory + "/" + update.getTitle() + "/";
        File file = new File(location);
        if (file.exists()) {
            recursiveDelete(file);
        }
        file.mkdir();
        (new File(location + "/js")).mkdir();
        (new File(location + "/CSS")).mkdir();
        (new File(location + "/media")).mkdir();
        Files.copy(getPath(EPortfolioConstants.PATH_BUILDER_SITE + "index.html"), getPath(location + "index.html"));
        Files.copy(getPath(EPortfolioConstants.PATH_BUILDER_SITE + "js/controller.js"), getPath(location + "js/controller.js"));
        File temp = new File(location + "js/file.json");
        OutputStream os = new FileOutputStream(temp);
        if (!file.exists()) {
            file.createNewFile();
        }

        try (JsonWriter writer = Json.createWriter(os)) {
            writer.writeObject(convertJson(update));
        }
        addMedia(directory + "/", update);
        temp = new File(EPortfolioConstants.PATH_BUILDER_SITE + "/CSS/");

        for (File cssCopy : temp.listFiles()) {
            Files.copy(cssCopy.toPath(),
                    getPath(location + "/CSS/" + cssCopy.getName()));
        }

        return new File(location + "index.html").toURI().toURL();
    }

    private static void addMedia(String directory, EPortfolioModel savings) throws IOException {
        String local = directory + savings.getTitle() + "/media/";
        (new File(savings.getBannerImage())).getName();
        if (!(new File(getLocalName(savings.getBannerImage(), local)).exists())) {
            Files.copy(getPath(savings.getBannerImage()), getPath(getLocalName(savings.getBannerImage(), local)));
        }
        for (Page page : savings.getPages()) {
            for (PageElement element : page.getElements()) {
                String localName;
                if (element instanceof ImageElement) {
                    localName = getLocalName(((ImageElement) element).getSourceFile(), local);
                    if (doesExist(localName)) {
                        continue;
                    }
                    Files.copy(getPath(((ImageElement) element).getSourceFile()), getPath(localName));
                } else if (element instanceof VideoElement) {
                    localName = getLocalName(((VideoElement) element).getSourceFile(), local);
                    if (doesExist(localName)) {
                        continue;
                    }
                    Files.copy(getPath(((VideoElement) element).getSourceFile()), getPath(localName));
                } else if (element instanceof SlideShowElement) {
                    for (Slide slide : ((SlideShowElement) element).getSlides()) {
                        localName = getLocalName(slide.getFileLocation(), local);
                        if (doesExist(localName)) {
                            continue;
                        }
                        Files.copy(getPath(slide.getFileLocation()), getPath(localName));
                    }
                }
            }
        }
    }

    private static boolean doesExist(String location) {
        return new File(location).exists();
    }

    private static Path getPath(String location) {
        return new File(location).toPath();
    }

    private static String getLocalName(String complete, String local) {
        String temp = new File(complete).getName();
        return local + temp;
    }

    private static void recursiveDelete(File file) {
        File[] contents = file.listFiles();
        if (contents != null) {
            for (File f : contents) {
                recursiveDelete(f);
            }
        }
        file.delete();
    }

    /**
     * This method loads the contents of a JSON file representing a slide show
     * into a SlideSShowModel objecct.
     *
     * @param ePortfolioToLoad The slide show to load
     * @param jsonFilePath The JSON file to load.
     * @throws IOException
     */
    public static void loadSlideShow(EPortfolioModel ePortfolioToLoad, String jsonFilePath) throws IOException {
        // LOAD THE JSON FILE WITH ALL THE DATA
        JsonObject json = loadJSONFile(jsonFilePath);

        ObservableList<Page> tempPages = FXCollections.observableArrayList();
        // NOW LOAD THE COURSE
        JsonArray jsonPageArray = json.getJsonArray(JSON_PAGES);
        JsonArray jsonElementArray;
        String comparator;
        JsonObject pageJso, elementJso;
        PageElement tempPageElement;

        for (int i = 0; i < jsonPageArray.size(); i++) {
            pageJso = jsonPageArray.getJsonObject(i);
            Page temp = new Page(pageJso.getString(JSON_TITLE));
            temp.setFooterText(pageJso.getString(JSON_FOOTER));
            temp.setColor(pageJso.getInt(JSON_COLOR));
            temp.setLayout(pageJso.getInt(JSON_LAYOUT));
            temp.setFont(pageJso.getInt(JSON_FONT));
            jsonElementArray = pageJso.getJsonArray(JSON_ELEMENTS);
            for (int j = 0; j < jsonElementArray.size(); j++) {
                elementJso = jsonElementArray.getJsonObject(j);
                comparator = elementJso.getString(JSON_DELIMITER_KEY);
                switch (comparator) {
                    case TEXT_DELIMITER:
                        tempPageElement = new TextElement();
                        ((TextElement) tempPageElement).setMessage(elementJso.getString(JSON_MESSAGE));
                        ((TextElement) tempPageElement).setFont(elementJso.getInt(JSON_FONT));
                        temp.addElement(tempPageElement);
                        break;
                    case IMAGE_DELIMITER:
                        tempPageElement = new ImageElement();
                        ((ImageElement) tempPageElement).setSourceFile(EPortfolioConstants.PATH_MEDIA + elementJso.getString(JSON_SOURCE));
                        ((ImageElement) tempPageElement).setCaption(elementJso.getString(JSON_CAPTION));
                        ((ImageElement) tempPageElement).setFloatLocation(elementJso.getInt(JSON_FLOAT));
                        ((ImageElement) tempPageElement).setHeight(elementJso.getInt(JSON_HEIGHT));
                        ((ImageElement) tempPageElement).setWidth(elementJso.getInt(JSON_WIDTH));
                        temp.addElement(tempPageElement);
                        break;
                    case VIDEO_DELIMITER:
                        tempPageElement = new VideoElement();
                        ((VideoElement) tempPageElement).setSourceFile(EPortfolioConstants.PATH_MEDIA + elementJso.getString(JSON_SOURCE));
                        ((VideoElement) tempPageElement).setCaption(elementJso.getString(JSON_CAPTION));
                        ((VideoElement) tempPageElement).setHeight(elementJso.getInt(JSON_HEIGHT));
                        ((VideoElement) tempPageElement).setWidth(elementJso.getInt(JSON_WIDTH));
                        temp.addElement(tempPageElement);
                        break;
                    case SLIDE_SHOW_DELIMITER:
                        tempPageElement = new SlideShowElement();
                        ArrayList<Slide> list = new ArrayList();
                        for (int k = 0; k < elementJso.getJsonArray(JSON_SLIDES).size(); k++) {
                            Slide tempSlide = new Slide();
                            JsonObject tempObject = elementJso.getJsonArray(JSON_SLIDES).getJsonObject(k);
                            tempSlide.setCaption(tempObject.getString(JSON_CAPTION));
                            tempSlide.setHeight(tempObject.getInt(JSON_HEIGHT));
                            tempSlide.setWidth(tempObject.getInt(JSON_WIDTH));
                            tempSlide.setFileLocation(EPortfolioConstants.PATH_MEDIA + tempObject.getString(JSON_SOURCE));
                            ((SlideShowElement) tempPageElement).getSlides().add(tempSlide);
                            temp.addElement(tempPageElement);
                        }
                        break;
                    case LIST_DELIMITER:
                        tempPageElement = new ListElement(elementJso.getString(JSON_TITLE));
                        ArrayList<String> tempList = new ArrayList();
                        for (int k = 0; k < elementJso.getJsonArray(JSON_SLIDES).size(); k++) {
                            tempList.add(elementJso.getJsonArray(JSON_SLIDES).getJsonObject(k).getString(JSON_MESSAGE));
                        }
                        for (String tempString : tempList) {
                            ((ListElement) tempPageElement).getList().add(tempString);
                        }
                        temp.addElement(tempPageElement);
                        break;
                    case HEADER_DELIMITER:
                        tempPageElement = new HeaderElement();
                        ((HeaderElement) tempPageElement).setMessage(elementJso.getString(JSON_MESSAGE));
                        temp.addElement(tempPageElement);
                        break;
                }
            }
            tempPages.add(temp);
            System.out.println("");

        }
        ePortfolioToLoad.updatePortfolio(json.getString(JSON_TITLE), json.getString(JSON_BANNER), json.getString(JSON_NAME), tempPages);
    }

    // AND HERE ARE THE PRIVATE HELPER METHODS TO HELP THE PUBLIC ONES
    private static JsonObject loadJSONFile(String jsonFilePath) throws IOException {
        InputStream is = new FileInputStream(jsonFilePath);
        JsonReader jsonReader = Json.createReader(is);
        JsonObject json = jsonReader.readObject();
        jsonReader.close();
        is.close();
        return json;
    }

    private static ArrayList<String> loadArrayFromJSONFile(String jsonFilePath, String arrayName) throws IOException {
        JsonObject json = loadJSONFile(jsonFilePath);
        ArrayList<String> items = new ArrayList();
        JsonArray jsonArray = json.getJsonArray(arrayName);
        for (JsonValue jsV : jsonArray) {
            items.add(jsV.toString());
        }
        return items;
    }

    private static JsonArray makePagesArray(List<Page> pages) {
        JsonArrayBuilder jsb = Json.createArrayBuilder();
        for (Page page : pages) {
            JsonObjectBuilder builder = Json.createObjectBuilder();
            JsonArray jso = makeElementsArray(page.getElements());
            builder.add(JSON_FONT, page.getFont());
            builder.add(JSON_LAYOUT, page.getLayout());
            builder.add(JSON_TITLE, page.getTitle());
            builder.add(JSON_COLOR, page.getColor());
            builder.add(JSON_FOOTER, page.getFooterText());
            builder.add(JSON_ELEMENTS, jso);
            jsb.add(builder.build());
        }
        JsonArray jA = jsb.build();
        return jA;
    }

    private static JsonArray makeElementsArray(List<PageElement> elements) {
        JsonArrayBuilder jsb = Json.createArrayBuilder();
        for (PageElement element : elements) {
            JsonObject jso = makeElementJsonObject(element);
            jsb.add(jso);
        }
        JsonArray jA = jsb.build();
        return jA;
    }

    //DELIMITERS
    public static String JSON_IMAGE_FILE_NAME = "image_file_name";
    public static String JSON_EXT = ".json";
    public static String SLASH = "/";
    public static String JSON_NAME = "name";
    public static String JSON_BANNER = "banner";
    public static String JSON_TITLE = "title";
    public static String JSON_LAYOUT = "layout";
    public static String JSON_COLOR = "color";
    public static String JSON_FOOTER = "footer";

    public static final String TEXT_DELIMITER = "paragraph";
    public static final String HEADER_DELIMITER = "header";
    public static final String LIST_DELIMITER = "list";
    public static final String IMAGE_DELIMITER = "image";
    public static final String VIDEO_DELIMITER = "video";
    public static final String SLIDE_SHOW_DELIMITER = "slideshow";
    public static String JSON_DELIMITER_KEY = "type";

    public static String JSON_CAPTION = "caption";
    public static String JSON_HEIGHT = "height";
    public static String JSON_WIDTH = "width";
    public static String JSON_MESSAGE = "message";
    public static String JSON_FONT = "font";
    public static String JSON_SOURCE = "source";
    public static String JSON_FLOAT = "float";
    public static String JSON_SLIDES = "slides";

    public static String JSON_IMAGE_PATH = "image_path";
    public static String JSON_TEXT_CAPTION = "caption_text";
    public static String JSON_PAGES = "pages";
    public static String JSON_ELEMENTS = "elements";

    private static JsonObject makeElementJsonObject(PageElement element) {
        JsonObjectBuilder builder = Json.createObjectBuilder();

        if (element instanceof TextElement) {
            builder.add(JSON_DELIMITER_KEY, TEXT_DELIMITER);
            builder.add(JSON_FONT, ((TextElement) element).getFont());
            builder.add(JSON_MESSAGE, ((TextElement) element).getMessage());
        } else if (element instanceof HeaderElement) {
            builder.add(JSON_DELIMITER_KEY, HEADER_DELIMITER);
            builder.add(JSON_MESSAGE, ((HeaderElement) element).getMessage());
        } else if (element instanceof ImageElement) {
            builder.add(JSON_DELIMITER_KEY, IMAGE_DELIMITER);
            builder.add(JSON_CAPTION, ((ImageElement) element).getCaption());
            builder.add(JSON_SOURCE, new File("file:" + ((ImageElement) element).getSourceFile()).getName());
            builder.add(JSON_HEIGHT, ((ImageElement) element).getHeight());
            builder.add(JSON_WIDTH, ((ImageElement) element).getHeight());
            builder.add(JSON_FLOAT, ((ImageElement) element).getFloatLocation());
        } else if (element instanceof VideoElement) {
            builder.add(JSON_DELIMITER_KEY, VIDEO_DELIMITER);
            builder.add(JSON_CAPTION, ((VideoElement) element).getCaption());
            builder.add(JSON_SOURCE, new File("file:" + ((VideoElement) element).getSourceFile()).getName());
            builder.add(JSON_HEIGHT, ((VideoElement) element).getHeight());
            builder.add(JSON_WIDTH, ((VideoElement) element).getHeight());
        } else if (element instanceof SlideShowElement) {
            builder.add(JSON_DELIMITER_KEY, SLIDE_SHOW_DELIMITER);
            JsonArrayBuilder jsb = Json.createArrayBuilder();
            for (Slide slide : ((SlideShowElement) element).getSlides()) {
                JsonObject jso = Json.createObjectBuilder()
                        .add(JSON_SOURCE, (new File("file:" + (slide.getFileLocation())).getName()))
                        .add(JSON_CAPTION, slide.getCaption())
                        .add(JSON_HEIGHT, slide.getHeight())
                        .add(JSON_WIDTH, slide.getWidth())
                        .build();
                jsb.add(jso);
            }
            JsonArray jA = jsb.build();
            builder.add(JSON_SLIDES, jA);
        } else if (element instanceof ListElement) {
            builder.add(JSON_DELIMITER_KEY, LIST_DELIMITER);
            JsonArrayBuilder jsb = Json.createArrayBuilder();
            builder.add(JSON_TITLE, ((ListElement) element).getTitle());
            for (String listElement : ((ListElement) element).getList()) {
                JsonObject jso = Json.createObjectBuilder()
                        .add(JSON_MESSAGE, listElement)
                        .build();
                jsb.add(jso);
            }
            JsonArray jA = jsb.build();
            builder.add(JSON_SLIDES, jA);
        }

        return builder.build();
    }
}
