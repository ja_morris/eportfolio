/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfolio;

import javafx.application.Application;
import javafx.stage.Stage;
//palleton pallet : 63+140knaRF00++aYTRHaV0++sp

//Palette UID: 53+140kwi++bu++hX++++rd++kX
/**
 *
 * @author James
 */
public class EPortfolio extends Application {

    private EPortfolioInterface ui;

    @Override
    public void start(Stage primaryStage) {
        ui = new EPortfolioInterface();
        ui.initUI(primaryStage);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
