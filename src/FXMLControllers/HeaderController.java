/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FXMLControllers;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author James
 */
public class HeaderController extends FXMLController implements Initializable {

    @FXML
    private Button btnOK;
    @FXML
    private Button btnCancel;
    @FXML
    private TextField textArea;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    @Override
    public Button getBtnOk() {
        return btnOK;
    }

    @Override
    public Button getBtnCancel() {
        return btnCancel;
    }

    public TextField getTextArea() {
        return textArea;
    }

    @Override
    public void initializeVariables(Object... initializers) {
        textArea.setText((String) initializers[0]);
    }

}
