/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FXMLControllers;

import eportfolio.EPortfolioConstants;
import static eportfolio.Elements.ElementDistinguisher.Image;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;

/**
 * FXML Controller class
 *
 * @author James
 */
public class ListController extends FXMLController implements Initializable {

    @FXML
    private VBox containerBox;
    @FXML
    private Button btnOk;
    @FXML
    private Button btnCancel;
    @FXML
    private TextField txtTitle;
    @FXML
    private Button btnAdd;
    @FXML
    private Button btnRemove;

    List<String> listOrign;
    ObservableList<Node> list;
    private boolean hasChanged;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    @Override
    public Button getBtnOk() {
        return btnOk;
    }

    @Override
    public Button getBtnCancel() {
        return btnCancel;
    }

    @Override
    public void initializeVariables(Object... initializers) {
        txtTitle.setText((String) initializers[0]);
        list = containerBox.getChildren();
        listOrign = (List<String>) initializers[1];
        for (String temp : listOrign) {
            list.add(new TextField(temp));
        }
        btnAdd.setOnAction(e -> {
            addElement();
        });
        btnRemove.setOnAction(e -> {
            removeElement();
        });
        ((ImageView) btnAdd.getGraphic()).setImage(new Image("file:" + EPortfolioConstants.PATH_ICONS + EPortfolioConstants.BUTTON_ADD_ELEMENT_LOCATION));
        ((ImageView) btnRemove.getGraphic()).setImage(new Image("file:" + EPortfolioConstants.PATH_ICONS + EPortfolioConstants.BUTTON_REMOVE_ELEMENT_LOCATION));
    }

    private void addElement() {
        list.add(new TextField(EPortfolioConstants.DEFAULT_LIST_ELEMENT_COMPONENT));
    }

    private void removeElement() {
        if (!list.isEmpty()) {
            list.remove(list.size() - 1);
        }
    }

    public boolean hasChanged() {
        if (list.size() != listOrign.size()) {
            return true;
        }
        for (int i = 0; i < list.size(); i++) {
            if (!((TextField) list.get(i)).getText().equals(listOrign.get(i))) {
                return true;
            }
        }
        return false;
    }

    public String getTitle() {
        return txtTitle.getText();
    }

    public List<String> getList() {
        ArrayList<String> temp = new ArrayList();
        for (int i = 0; i < list.size(); i++) {
            temp.add(((TextField) list.get(i)).getText());
        }
        return temp;
    }
}
