/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FXMLControllers;

import javafx.scene.control.Button;

/**
 *
 * @author James
 */
public abstract class FXMLController {

    public abstract void initializeVariables(Object... initializers);

    public abstract Button getBtnOk();

    public abstract Button getBtnCancel();
}
