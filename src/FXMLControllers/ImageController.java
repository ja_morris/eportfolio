/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FXMLControllers;

import eportfolio.Controller.GeneralSelectionController;
import eportfolio.Controller.ImageSelectionController;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;

/**
 * FXML Controller class
 *
 * @author James
 */
public class ImageController extends FXMLController implements Initializable {

    @FXML
    private ImageView selectImg;
    @FXML
    private Button btnOk;
    @FXML
    private Button btnCancel;
    @FXML
    private TextField txtWidth;
    @FXML
    private TextField txtHeight;
    @FXML
    private MenuButton btnMenu;
    @FXML
    private TextArea txtCaption;

    private static String FLOAT_STRING = "Alignment: ";
    private String location;
    private int floatLocation;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    private void loadImage() {
        selectImg.setImage(new Image("file:" + location));
    }

    private void updateMenu() {
        btnMenu.setText(FLOAT_STRING + btnMenu.getItems().get(floatLocation).getText());
    }

    @Override
    public void initializeVariables(Object... initializers) {
        location = (String) initializers[0];
        txtCaption.setText((String) initializers[1]);
        floatLocation = (Integer) initializers[2];
        for (int i = 0; i < btnMenu.getItems().size(); i++) {
            btnMenu.getItems().get(i).setOnAction(e -> {
                floatLocation = btnMenu.getItems().indexOf(e.getSource());
                updateMenu();
            });
        }
        selectImg.addEventHandler(MouseEvent.MOUSE_CLICKED, e -> {
            String temp;
            GeneralSelectionController controller = new GeneralSelectionController();
            if ((temp = controller.processSelectImage("")) == null) {
                return;
            } else {
                location = temp;
            }
            loadImage();
        });
        txtHeight.setText((String.valueOf((Integer) initializers[3])));
        txtWidth.setText((String.valueOf((Integer) initializers[4])));
        updateMenu();
        loadImage();
    }

    @Override
    public Button getBtnOk() {
        return btnOk;
    }

    @Override
    public Button getBtnCancel() {
        return btnCancel;
    }

    public String getLocation() {
        return location;
    }

    public int getFloatLocation() {
        return floatLocation;
    }

    public String getCaption() {
        return txtCaption.getText();
    }

    public int getHeight() {
        return Integer.parseInt(txtHeight.getText());
    }

    public int getWidth() {
        return Integer.parseInt(txtWidth.getText());
    }

}
