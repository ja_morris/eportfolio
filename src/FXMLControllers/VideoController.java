/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FXMLControllers;

import eportfolio.Controller.GeneralSelectionController;
import eportfolio.Controller.VideoSelectionController;
import eportfolio.Elements.VideoElement;
import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.MenuButton;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;

/**
 * FXML Controller class
 *
 * @author James
 */
public class VideoController extends FXMLController implements Initializable {

    @FXML
    private Button btnOk;
    @FXML
    private Button btnCancel;
    @FXML
    private TextField txtWidth;
    @FXML
    private TextField txtHeight;
    @FXML
    private TextArea txtCaption;
    @FXML
    private MediaView vidSelect;

    String location;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    @Override
    public void initializeVariables(Object... initializers) {
        location = (String) initializers[0];
        txtCaption.setText((String) initializers[1]);
        vidSelect.addEventHandler(MouseEvent.MOUSE_CLICKED, e -> {
            VideoSelectionController controller = new VideoSelectionController();
            VideoElement tempElement = new VideoElement();
            controller.processSelectImage(tempElement);
            if (tempElement.getTempSourceFile() == null) {
                return;
            } else {
                location = tempElement.getTempSourceFile();
            }
            loadVideo();
        });
        vidSelect.setOnMouseEntered(e -> {
            vidSelect.getMediaPlayer().play();
        });
        vidSelect.setOnMouseExited(e -> {
            vidSelect.getMediaPlayer().pause();
        });
        txtHeight.setText((String.valueOf((Integer) initializers[2])));
        txtWidth.setText((String.valueOf((Integer) initializers[3])));
        loadVideo();
    }

    private void loadVideo() {
        vidSelect.setMediaPlayer(new MediaPlayer(this.generateMedia()));
    }

    @Override
    public Button getBtnOk() {
        return btnOk;
    }

    @Override
    public Button getBtnCancel() {
        return btnCancel;
    }

    public String getLocation() {
        return location;
    }

    public String getCaption() {
        return txtCaption.getText();
    }

    public int getHeight() {
        return Integer.parseInt(txtHeight.getText());
    }

    public int getWidth() {
        return Integer.parseInt(txtWidth.getText());
    }

    private Media generateMedia() {
        if (this.location.toLowerCase().contains("http")) {
            return new Media(this.location);
        } else {
            return new Media(new File(this.location).toURI().toString());
        }
    }

}
