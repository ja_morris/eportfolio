/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FXMLControllers;

import eportfolio.Controller.GeneralSelectionController;
import eportfolio.Model.EPortfolioModel;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.MenuButton;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;

/**
 * FXML Controller class
 *
 * @author James
 */
public class PageInformationController implements Initializable {

    @FXML
    private ImageView imgBanner;
    @FXML
    private TextField txtTitleMain;
    @FXML
    private TextField txtTitlePage;
    @FXML
    private MenuButton btnFont;
    @FXML
    private MenuButton btnLayout;
    @FXML
    private TextField txtStudent;
    @FXML
    private TextField txtFooter;
    @FXML
    private MenuButton btnColor;
    @FXML
    private Button btnConfirm;

    private EPortfolioModel portfolio;
    private int color, layout, font;
    private String location;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    public void updateMenu() {
        btnColor.setText("Color: " + btnColor.getItems().get(color).getText());
        btnLayout.setText("Layout: " + btnLayout.getItems().get(layout).getText());
        btnFont.setText("Font: " + btnFont.getItems().get(font).getText());
    }

    public void initializeVariables(Object... initializer) {
        portfolio = (EPortfolioModel) initializer[0];
        location = portfolio.getBannerImage();
        font = portfolio.getCurrentPageSelected().getFont();
        layout = portfolio.getCurrentPageSelected().getLayout();
        color = portfolio.getCurrentPageSelected().getColor();
        txtStudent.setText(portfolio.getStudentName());
        txtFooter.setText(portfolio.getCurrentPageSelected().getFooterText());
        txtTitlePage.setText(portfolio.getCurrentPageSelected().getTitle());
        txtTitleMain.setText(portfolio.getTitle());

        for (int i = 0; i < btnLayout.getItems().size(); i++) {
            btnColor.getItems().get(i).setOnAction(e -> {
                color = btnColor.getItems().indexOf(e.getSource());
                updateMenu();
            });
            btnLayout.getItems().get(i).setOnAction(e -> {
                layout = btnLayout.getItems().indexOf(e.getSource());
                updateMenu();
            });
            btnFont.getItems().get(i).setOnAction(e -> {
                font = btnFont.getItems().indexOf(e.getSource());
                updateMenu();
            });
        }
        imgBanner.addEventHandler(MouseEvent.MOUSE_CLICKED, e -> {
            String temp;
            GeneralSelectionController controller = new GeneralSelectionController();
            if ((temp = controller.processSelectImage("")) == null) {
                return;
            } else {
                location = temp;
            }
            loadImage();
        });
        updateMenu();
        loadImage();
        btnConfirm.setOnAction(e -> {
            portfolio.setBannerImage(location);
            portfolio.setStudentName(txtStudent.getText());
            portfolio.setTitle(txtTitleMain.getText());
            portfolio.getCurrentPageSelected().setTitle(txtTitlePage.getText());
            portfolio.getCurrentPageSelected().setFont(font);
            portfolio.getCurrentPageSelected().setColor(color);
            portfolio.getCurrentPageSelected().setLayout(layout);
            portfolio.getUI().getWorkSpace().paint();
        });
    }

    private void loadImage() {
        imgBanner.setImage(new Image("file:" + location));
    }
}
