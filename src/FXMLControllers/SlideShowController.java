/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FXMLControllers;

import eportfolio.EPortfolioConstants;
import eportfolio.Elements.HeaderElement;
import eportfolio.Elements.SlideElements.Slide;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;

/**
 * FXML Controller class
 *
 * @author James
 */
public class SlideShowController extends FXMLController implements Initializable {

    @FXML
    private VBox containerBox;
    @FXML
    private Button btnOk;
    @FXML
    private Button btnCancel;
    @FXML
    private Button btnAdd;
    @FXML
    private Button btnRemove;
    @FXML
    private Button btnMoveUp;
    @FXML
    private Button btnMoveDown;

    private ArrayList<SlideController> slides;
    private SlideController currentSelected;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    @Override
    public void initializeVariables(Object... initializers) {
        slides = new ArrayList();
        ((ImageView) btnAdd.getGraphic()).setImage(new Image("file:" + EPortfolioConstants.PATH_ICONS + EPortfolioConstants.BUTTON_ADD_ELEMENT_LOCATION));
        ((ImageView) btnRemove.getGraphic()).setImage(new Image("file:" + EPortfolioConstants.PATH_ICONS + EPortfolioConstants.BUTTON_REMOVE_ELEMENT_LOCATION));
        ((ImageView) btnMoveUp.getGraphic()).setImage(new Image("file:" + EPortfolioConstants.PATH_ICONS + EPortfolioConstants.BUTTON_MOVEUP_ELEMENT_LOCATION));
        ((ImageView) btnMoveDown.getGraphic()).setImage(new Image("file:" + EPortfolioConstants.PATH_ICONS + EPortfolioConstants.BUTTON_MOVEDOWN_ELEMENT_LOCATION));
        btnAdd.setOnAction(e -> {
            this.addElement(new Slide());
            paintSlides();
        });
        btnRemove.setOnAction(e -> {
            this.removeElement();
            paintSlides();
        });
        btnMoveUp.setOnAction(e -> {
            this.moveElementUp();
            paintSlides();
        });
        btnMoveDown.setOnAction(e -> {
            this.moveElementDown();
            paintSlides();
        });
        slides = new ArrayList();
        for (Slide temp : (ArrayList<Slide>) initializers[0]) {
            addElement(temp);
        }
        paintSlides();
    }

    public void addElement(Slide temp) {
        AnchorPane pane;
        FXMLLoader loader;
        SlideController headController;
        try {
            pane = (loader = new FXMLLoader(getClass().getResource("/FXMLControllers/Slide.fxml"))).load();
            headController = loader.getController();
        } catch (IOException ex) {
            Logger.getLogger(HeaderElement.class.getName()).log(Level.SEVERE, null, ex);
            return;
        }
        headController.initializeVariables(temp, pane);

        currentSelected = headController;
        slides.add(headController);
    }

    public boolean removeElement() {
        if (!slides.isEmpty()) {
            SlideController tempElement = this.currentSelected;
            int location = slides.indexOf(tempElement);
            slides.remove(tempElement);
            if (location >= slides.size()) {
                location = slides.size() - 1;
            }
            if (slides.isEmpty()) {
                this.currentSelected = null;
            } else {
                this.currentSelected = slides.get(location);
            }
            return true;
        }
        return false;
    }

    public boolean moveElementUp() {
        if (!slides.isEmpty()) {
            SlideController temp = this.currentSelected;
            int locationOfTemp = slides.indexOf(temp);
            if (locationOfTemp > 0) {
                slides.set(locationOfTemp, slides.get(locationOfTemp - 1));
                slides.set(locationOfTemp - 1, temp);
                return true;
            }
        }
        return false;
    }

    public boolean moveElementDown() {
        if (!slides.isEmpty()) {
            SlideController temp = this.currentSelected;
            int locationOfTemp = slides.indexOf(temp);
            if (locationOfTemp < slides.size() - 1) {
                slides.set(locationOfTemp, slides.get(locationOfTemp + 1));
                slides.set(locationOfTemp + 1, temp);
                return true;
            }
        }
        return false;
    }

    private void paintSlides() {
        if (slides.isEmpty()) {
            containerBox.getChildren().clear();
            return;
        }
        containerBox.getChildren().clear();
        for (SlideController temp : slides) {
            AnchorPane pane = temp.getPane();
            pane.addEventHandler(MouseEvent.MOUSE_CLICKED, e -> {
                if (currentSelected != null && !this.currentSelected.equals(temp)) {
                    this.currentSelected = temp;
                    paintSlides();
                }
            });
            if (temp.equals(this.currentSelected)) {
                pane.setStyle("-fx-border-color: blue;\n"
                        + "-fx-border-insets: 5;\n"
                        + "-fx-border-width: 3;\n"
                        + "-fx-border-style: dashed;\n");
            } else {
                pane.setStyle("-fx-border-color: black;\n"
                        + "-fx-border-insets: 5;\n"
                        + "-fx-border-width: 3;\n"
                        + "-fx-border-style: solid;\n");
            }
            containerBox.getChildren().add(pane);
        }
    }

    public ArrayList<Slide> getSlides() {
        ArrayList<Slide> tmp = new ArrayList();
        for (SlideController tempControl : slides) {
            tmp.add(tempControl.getSlide());
        }
        return tmp;
    }

    @Override
    public Button getBtnOk() {
        return btnOk;
    }

    @Override
    public Button getBtnCancel() {
        return btnCancel;
    }

}
