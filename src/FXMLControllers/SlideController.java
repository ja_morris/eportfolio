/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FXMLControllers;

import eportfolio.Controller.SlideSelectionController;
import eportfolio.Elements.SlideElements.Slide;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;

/**
 * FXML Controller class
 *
 * @author James
 */
public class SlideController implements Initializable {

    @FXML
    private ImageView imgSelect;
    @FXML
    private TextField txtHeight;
    @FXML
    private TextField txtWidth;
    @FXML
    private TextArea txtCaption;

    private Slide newSlide;
    private AnchorPane pane;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    public void initializeVariables(Object... initializers) {
        Slide current = (Slide) initializers[0];
        pane = (AnchorPane) initializers[1];
        newSlide = new Slide();
        newSlide.setFileLocation(current.getFileLocation());
        newSlide.setCaption(current.getCaption());
        newSlide.setHeight(current.getHeight());
        newSlide.setWidth(current.getWidth());

        imgSelect.addEventHandler(MouseEvent.MOUSE_CLICKED, e -> {
            SlideSelectionController controller = new SlideSelectionController();
            controller.processSelectImage(newSlide);
            updateImage();
        });
        txtCaption.textProperty().addListener((observable, oldValue, newValue) -> {
            newSlide.setCaption(newValue);
        });
        txtHeight.textProperty().addListener((observable, oldValue, newValue) -> {
            newSlide.setHeight(new Integer(newValue));
        });
        txtWidth.textProperty().addListener((observable, oldValue, newValue) -> {
            newSlide.setWidth(new Integer(newValue));
        });
        updateImage();
    }

    private void updateImage() {
        imgSelect.setImage(new Image("file:" + this.newSlide.getFileLocation()));
    }

    public Slide getSlide() {
        return newSlide;
    }

    public AnchorPane getPane() {
        return this.pane;
    }
}
