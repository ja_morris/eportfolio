/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FXMLControllers;

import eportfolio.EPortfolioConstants;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author James
 */
public class TextController extends FXMLController implements Initializable {

    @FXML
    private TextArea txtMessage;
    @FXML
    private MenuButton btnFonts;
    @FXML
    private Button btnCancel;
    @FXML
    private Button btnOk;
    @FXML
    private TextField txtLike;
    @FXML
    private Button btnAddLink;

    int fontLocal;
    private static final String FONT_PREFIX = "Fonts: ";

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    private void updateFontText() {
        btnFonts.setText(FONT_PREFIX + EPortfolioConstants.FONT_TYPES[fontLocal]);
    }

    @Override
    public void initializeVariables(Object... initializers) {
        for (int i = 0; i < EPortfolioConstants.FONT_TYPES.length; i++) {
            MenuItem temp = new MenuItem(EPortfolioConstants.FONT_TYPES[i]);
            temp.setOnAction(e -> {
                fontLocal = btnFonts.getItems().indexOf(e.getSource());
                updateFontText();
            });
            btnFonts.getItems().add(temp);
        }
        txtMessage.setText((String) initializers[0]);
        fontLocal = (Integer) initializers[1];
        updateFontText();
    }

    public int getFontLocation() {
        return fontLocal;
    }

    public String getMessage() {
        return txtMessage.getText();
    }

    @Override
    public Button getBtnOk() {
        return btnOk;
    }

    @Override
    public Button getBtnCancel() {
        return btnCancel;
    }

}
