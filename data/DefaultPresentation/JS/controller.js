var pageNumber = 1;
var totalPages;
var parsedJSON, parsedPageJSON;
var colorIntentStyles = ["CSS/style1.css", "CSS/style2.css", 
    "CSS/style3.css", "CSS/style4.css", "CSS/style5.css"];
var formatIntentStyles = ["CSS/format1.css", "CSS/format2.css", 
    "CSS/format3.css", "CSS/format4.css", "CSS/format5.css"];
var playInterval = 3000;

function controller(){
    parsedJSON = mimickJSONRead();
    parseHeaderElements(parsedJSON);
}

function parseHeaderElements(parsedJSON){
    var headerElements = parsedJSON[0];
    document.title = headerElements[0];
    generateBanner(headerElements[1], headerElements[2]);
    totalPages = headerElements[3];
    var temp = [];
    var i;
    for(i = 1; i < parsedJSON.length; i++)
        temp[i-1] = parsedJSON[i];
    parsedPageJSON = temp;
    generateNavigation(headerElements[4], headerElements[5]);
}

function generateBanner( name, source ){
    var container = document.getElementById("banner");
    var genBanner = document.createElement("h1");
    genBanner.innerHTML = name;
    genBanner.className = "banner";
    container.appendChild(genBanner); 
    var imgContainer = document.getElementById("bannerIMG");
    var generatedImg = document.createElement("img");
    generatedImg.className = "bannerIMG";
    generatedImg.src = source;
    imgContainer.appendChild(generatedImg);
}


function mimickJSONRead(){
    var temp = [
        ["ePortfolio", "Welcome, my name is James Morris, and this is my site.", "JS/testbanner.jpg", 5, ["First Style", "Second Style", "Third Style", "Fourth Style", "Fifth Style"], [0, 1, 2, 4, 3]],
        [ ["list", "Cats (This is a list of some cats I see on the street<br> and some I don't wish to see ie. the lion )", "black cats", ["Lions", "https://en.wikipedia.org/wiki/Lion"], "cats with spots", "orange cats", "fat cats", "small cats", "big cats"], 
        ["vid", "JS/test.mp4", "this is a video I found from vimeo on their homepage."], ["par", "This is a message element", ["it can contain a link", "https://www.google.com"], "followed by text",["followed by even more links", "http://www.cnn.com"]],
        ["img", "JS/test1.jpg", "This is the mona lisa"], ["header", "this is a header, it can isolate different elements with a categorizing effect"], ["slide", ["JS/test2.jpg", "JS/test3.jpg"], ["This is blueberry pie", "This is apple pie"] ] ],
        
        [ ["vid", "JS/test.mp4", "this is a video I found from vimeo on their homepage."], ["par", "This is a message element", ["it can contain a link", "https://www.google.com"], "followed by text",["followed by even more links", "http://www.cnn.com"]],
        ["img", "JS/test1.jpg", "This is the mona lisa"], ["list", "Cats (This is a list of some cats I see on the street<br> and some I don't wish to see ie. the lion )", "black cats", ["Lions", "https://en.wikipedia.org/wiki/Lion"], "cats with spots", "orange cats", "fat cats", "small cats", "big cats"], ["header", "this is a header, it can isolate different elements with a categorizing effect"], ["slide", ["JS/test2.jpg", "JS/test3.jpg"], ["This is blueberry pie", "This is apple pie"] ] ],
        
        [ ["list", "Cats (This is a list of some cats I see on the street<br> and some I don't wish to see ie. the lion )", "black cats", ["Lions", "https://en.wikipedia.org/wiki/Lion"], "cats with spots", "orange cats", "fat cats", "small cats", "big cats"], 
        ["vid", "JS/test.mp4", "this is a video I found from vimeo on their homepage."], ["par", "This is a message element", ["it can contain a link", "https://www.google.com"], "followed by text",["followed by even more links", "http://www.cnn.com"]],
        ["img", "JS/test1.jpg", "This is the mona lisa"], ["header", "this is a header, it can isolate different elements with a categorizing effect"], ["slide", ["JS/test2.jpg", "JS/test3.jpg"], ["This is blueberry pie", "This is apple pie"] ] ],
        
        [ ["list", "Cats (This is a list of some cats I see on the street<br> and some I don't wish to see ie. the lion )", "black cats", ["Lions", "https://en.wikipedia.org/wiki/Lion"], "cats with spots", "orange cats", "fat cats", "small cats", "big cats"], 
        ["vid", "JS/test.mp4", "this is a video I found from vimeo on their homepage."], ["par", "This is a message element", ["it can contain a link", "https://www.google.com"], "followed by text",["followed by even more links", "http://www.cnn.com"]],
        ["img", "JS/test1.jpg", "This is the mona lisa"], ["header", "this is a header, it can isolate different elements with a categorizing effect"], ["slide", ["JS/test2.jpg", "JS/test3.jpg"], ["This is blueberry pie", "This is apple pie"] ] ],
        
        [ ["list", "Cats (This is a list of some cats I see on the street<br> and some I don't wish to see ie. the lion )", "black cats", ["Lions", "https://en.wikipedia.org/wiki/Lion"], "cats with spots", "orange cats", "fat cats", "small cats", "big cats"], 
        ["vid", "JS/test.mp4", "this is a video I found from vimeo on their homepage."], ["par", "This is a message element", ["it can contain a link", "https://www.google.com"], "followed by text",["followed by even more links", "http://www.cnn.com"]],
        ["img", "JS/test1.jpg", "This is the mona lisa"], ["header", "this is a header, it can isolate different elements with a categorizing effect"], ["slide", ["JS/test2.jpg", "JS/test3.jpg"], ["This is blueberry pie", "This is apple pie"] ] ]    
    ];
    return temp;
}

function generateNavigation(pageName, colorIntent){
    var container = document.getElementById("navigation");
    var generateUL = document.createElement("UL");
    generateUL.id = "navigationMenu";
    var i;
    var iterate = 0;
    for(i = 0; i<totalPages; i++){
        iterate = createPageLinks(generateUL, iterate, pageName, colorIntent);
    }
    var nav = document.createElement("NAV");
    nav.appendChild(generateUL);
    container.appendChild(nav);
}

function createPageLinks(generateUL, iterate, pageName, colorIntent){
    var tempObject = {
        pageName: 0,
        colorIntent: 0,
        pageNum: 0,
        change: function(){
            changePage(this.pageNum, this.colorIntent);
        }
    };
    tempObject.colorIntent = colorIntent[iterate];
    tempObject.pageNum = iterate;
    var temp = document.createElement("A");
    temp.onclick = function(){
        var current = tempObject;
        current.change();
    };
    temp.innerHTML = pageName[iterate];
    var tempLink = document.createElement("LI");
    tempLink.appendChild(temp);
    generateUL.appendChild(tempLink);
    if(iterate===0)
        tempObject.change();
    return (iterate+1);
}

function changePage(pageNumber, colorIntent){
    if(!(this.pageNumber === pageNumber)){
        this.pageNumber = pageNumber;
        document.getElementById("styleSheet").setAttribute('href', colorIntentStyles[colorIntent]);
        if(colorIntent === 1){
            var bannerIMG = document.getElementById("bannerIMG");
            var banner = document.getElementById("banner");
            var nav = document.getElementById("navigation");
            nav.className = "navigation";
            var overall = document.getElementById("menu");
            overall.innerHTML = "";
            overall.appendChild(banner);
            overall.appendChild(nav);
            overall.appendChild(bannerIMG);
        }
        else {
            var bannerIMG = document.getElementById("bannerIMG");
            var banner = document.getElementById("banner");
            var nav = document.getElementById("navigation");
            var overall = document.getElementById("menu");
            overall.innerHTML = "";
            overall.appendChild(nav);
            overall.appendChild(banner);
            overall.appendChild(bannerIMG);
        }
        generateContent(parsedPageJSON);
    }
}

function generateContent(parsedElements){
    document.getElementById("content").innerHTML = "";
    var j;
    for(j = 0; j<parsedElements[pageNumber].length; j++){
        if(parsedElements[pageNumber][j][0] === "par"){
            var tempChange = [];
            var x = 0;
            for(x = 1; x < parsedElements[pageNumber][j].length; x++){
                tempChange[x-1] = parsedElements[pageNumber][j][x];
            }
            generateParagraph(tempChange);
        }
        else if(parsedElements[pageNumber][j][0] === "vid"){
            generateVideo(parsedElements[pageNumber][j][1], parsedElements[pageNumber][j][2]);
        }
        else if(parsedElements[pageNumber][j][0] === "img"){
            generateImage(parsedElements[pageNumber][j][1], parsedElements[pageNumber][j][2]);
        }
        else if(parsedElements[pageNumber][j][0] === "slide"){
            generateSlideShow(parsedElements[pageNumber][j][1], parsedElements[pageNumber][j][2]);
        }
        else if(parsedElements[pageNumber][j][0] === "header")
            generateHeader(parsedElements[pageNumber][j][1]);
        else if(parsedElements[pageNumber][j][0] === "list"){
            var tempChange = [];
            var x = 0;
            for(x = 2; x < parsedElements[pageNumber][j].length; x++){
                tempChange[x-2] = parsedElements[pageNumber][j][x];
            }
            generateList(parsedElements[pageNumber][j][1], tempChange);
        }
    }
    console.log(document.getElementById("content").innerHTML);
}

function generateList(title, message){
    var generatedDiv = document.createElement("div");
    var titleElement = document.createElement("h4");
    titleElement.innerHTML = title;
    generatedDiv.appendChild(titleElement);
    var generatedPar = document.createElement("p");
    var parsed = "";
    var i;
    for(i = 0; i<message.length; i++){
        if(typeof message[i] === "string"){
            parsed += " " +(i+1) +".\t      "+message[i] +  "<br>";
        }
        else{
            var anchorGen = document.createElement("A");
            anchorGen.setAttribute('href', message[i][1]);
            anchorGen.innerHTML = message[i][0];
            parsed += "<a href=\""+message[i][1]+"\" target=\"_blank\"> " + (i+1) + " " + message[i][0]+"</a><br>";
        }
    }
    generatedPar.innerHTML = parsed;
    generatedPar.className = "paragraph";
    generatedDiv.appendChild(generatedPar);
    
    var container = document.getElementById("content");
    container.appendChild(generatedDiv);
}

function generateHeader(message){
    var generatedDiv = document.createElement("div");
    var generatedPar = document.createElement("h2");
    generatedPar.innerHTML = message;
    generatedDiv.appendChild(generatedPar);
    var container = document.getElementById("content");
    container.appendChild(generatedDiv);
}

function generateParagraph(message){
    //Create Elements
    var generatedDiv = document.createElement("div");
    var generatedPar = document.createElement("p");
    //Personalize Elements
    var parsed = "";
    var i;
    for(i = 0; i<message.length; i++){
        if(typeof message[i] === "string"){
            parsed += " "+ message[i];
        }
        else{
            var anchorGen = document.createElement("A");
            anchorGen.setAttribute('href', message[i][1]);
            anchorGen.innerHTML = message[i][0];
            parsed += "<a href=\""+message[i][1]+"\" target=\"_blank\"> " + message[i][0]+"</a>";
        }
    }
    generatedPar.innerHTML = parsed;
    generatedPar.className = "paragraph";
    generatedDiv.appendChild(generatedPar);
    //Push Elements to HTML
    var container = document.getElementById("content");
    container.appendChild(generatedDiv);
}

function generateImage(address, caption){
    var generatedDiv = document.createElement("div");
    var generatedPar = document.createElement("p");
    var generatedImg = document.createElement("img");
    //Personalize CaptionElements
    generatedPar.innerHTML = caption;
    generatedPar.className = "paragraph";
    //Establish ImageElements
    generatedImg.src = address;
    generatedImg.className = "image";
    //Push Elements to HTML
    generatedDiv.appendChild(generatedImg);
    generatedDiv.appendChild(generatedPar);
    
    var container = document.getElementById("content");
    container.appendChild(generatedDiv);
}

function generateVideo(address, caption){
    var generatedDiv = document.createElement("div");
    var generatedPar = document.createElement("p");
    var generatedVid = document.createElement("video");
    //Personalize CaptionElements
    generatedPar.innerHTML = caption;
    generatedPar.className = "paragraph";
    //Personalize VideoElements
    generatedVid.src = address;
    generatedVid.loop = true;
    generatedVid.controls = true;
    generatedVid.className = "image";
    //Push Elements to HTML
    generatedDiv.appendChild(generatedVid);
    generatedDiv.appendChild(generatedPar);
    var container = document.getElementById("content");
    container.appendChild(generatedDiv);
}

function generateSlideShow(address, caption){
    var generatedDiv = document.createElement("div");
    var generatedBtnLeft = document.createElement("button");
    var generatedBtnRight = document.createElement("button");
    var generatedBtnPlay = document.createElement("button");
    var containerDiv = document.createElement("div");
    var generatedElements = [];
    var i;
    var numberOfSlides = address.length;
    
    for(i = 0; i<numberOfSlides; i++){
        var div = document.createElement("div");
        var par = document.createElement("p");
        var img = document.createElement("img");
        par.innerHTML = caption[i];
        par.className = "paragraph";
        img.src = address[i];
        img.className = "slide";
        div.appendChild(img);
        div.appendChild(par);
        div.className = "slide";
        generatedElements[i] = div;
    }
    generatedBtnLeft.innerHTML = "<-";
    generatedBtnPlay.innerHTML = "&#9658";
    generatedBtnRight.innerHTML = "->";
    
    var tempSlideshow = {
        currentSlide: 0,
        currentInterval: 0,
        playButton: 0,
        isPlaying: 0,
        totalSlide: numberOfSlides,
        arraySlide: [],
        container: containerDiv,
        fireContinue: function(){
          this.moveRight();  
        },
        moveLeft: function(){
            if(this.currentSlide === 0)
                this.currentSlide = this.totalSlide - 1;
            else
                this.currentSlide --;
            this.affectChange();
        },
        moveRight: function(){
            if(this.currentSlide === this.totalSlide -1)
                this.currentSlide = 0;
            else
                this.currentSlide++;
            this.affectChange();
        },
        affectChange: function(){
            this.container.innerHTML = (this.arraySlide[this.currentSlide]).innerHTML;
        }
    };
    tempSlideshow.playButton = generatedBtnPlay;
    tempSlideshow.isPlaying = 0;
    tempSlideshow.currentSlide = 0;
    tempSlideshow.totalSlide = numberOfSlides;
    tempSlideshow.arraySlide = generatedElements;
    tempSlideshow.container = containerDiv;
    containerDiv.innerHTML = generatedElements[0].innerHTML;
    generatedBtnLeft.onclick = 
        function(){
            var temp = tempSlideshow;
            temp.moveLeft();
            console.log(temp.currentSlide);
        };
    generatedBtnRight.onclick = 
        function(){
            var temp = tempSlideshow;
            temp.moveRight();
        };
    generatedBtnPlay.onclick = 
        function(){
            var temp = tempSlideshow;
            if(temp.isPlaying === 0){
                temp.playButton.innerHTML = "| |";
                temp.currentInterval = setInterval(function(){temp.fireContinue();}, playInterval);
                temp.isPlaying = 1;
                console.log("fired");
            }
            else{
                temp.playButton.innerHTML = "&#9658";
                temp.isPlaying = 1;
                clearInterval(temp.currentInterval);
            }
        };
    generatedDiv.appendChild(containerDiv);
    generatedDiv.appendChild(generatedBtnLeft);
    generatedDiv.appendChild(generatedBtnPlay);
    generatedDiv.appendChild(generatedBtnRight);
    
    var container = document.getElementById("content");
    container.appendChild(generatedDiv);
}