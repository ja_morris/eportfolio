# README #

Overview:
This program was first developed during my enrollment in CSE219 (Intermediate Java). However, after the conclusion of the class, development continued. The program allows users to develop an Electronic Portfolio comprised of elements (See Element Types) and then generate the corresponding HTML/JS/CSS file(s) along with the media data. This is meant to be deployed on a static web hosting site.

Elements:
The current elements able to be deployed are : Plain text, Header Text, Images, Videos, Slideshow (Images and corresponding captions), Lists.